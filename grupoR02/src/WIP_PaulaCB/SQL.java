/**
 * 
 */
package WIP_PaulaCB;

/**
 * @author root
 *
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class SQL {

	
	private static Connection Conexion = null;


	//M�TODO PERSONALIZADO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
         
	
	//M�TODO QUE FINALIZA LA CONEXION A SQL
	public void closeConnection() {
	        try {
	            Conexion.close();
	            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	           
	            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
	        }
	    }
    
	
	
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
	 public void createTable(String name) {
	        try {
	            String Query = "CREATE TABLE " + name + ""
	                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
	                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	 
     //METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
	 public void insertData(String table_name, String ID, String name, String lastname, String age, String gender) {
	        try {
	            String Query = "INSERT INTO " + table_name + " VALUES("
	            		+ "'"+ ID + "',"
	            		+ "'"+ name + "',"
	            		+ "'"+ lastname + "',"
	            		+ "'"+ age + "',"
	            		+ "'"+ gender + "')";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	    }	
	
	//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
	 public void getValues(String table_name) {
	        try {
	            String Query = "SELECT * FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println("ID: " + resultSet.getString("ID") + " "
	                        + "Nombre: " + resultSet.getString("Nombre") + " " 
							+ resultSet.getString("Apellido") + " "
	                        + "Edad: " + resultSet.getString("Edad") + " "
	                        + "Sexo: " + resultSet.getString("Sexo"));
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
	    }	
		
	    //METODO QUE INSERTA VALORES EN TABLA ENTRENADORES
	 public void insertDataEntrenadores( String DNI,String NOMBRE, String APELLIDO1, String APELLIDO2,String DIRECCION,String TELEFONO,String CUENTACORRIENTE) {
		 String table_name;
		 table_name="ENTRENADOR";
		 try {
	            String Query = "INSERT INTO " + table_name + " VALUES("
	            		+ "'"+ DNI + "',"
	            		+ "'"+ NOMBRE + "',"
	            		+ "'"+ APELLIDO1 + "',"
	            		+ "'"+ APELLIDO2 + "',"
	            		+ "'"+ DIRECCION + "',"
	            		+ "'"+ TELEFONO + "',"
	            		+ "'"+ CUENTACORRIENTE + "')";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	 }
		 //METODO QUE INSERTA VALORES EN TABLA CLIENTES
		 public void insertDataClientes( String DNI,String NOMBRE, String APELLIDO1, String APELLIDO2,String DIRECCION,String TELEFONO,String CUENTACORRIENTE) {
			 String table_name;
			 table_name="CLIENTE";
			 try {
		            String Query = "INSERT INTO " + table_name + " VALUES("
		            		+ "'"+ DNI + "',"
		            		+ "'"+ NOMBRE + "',"
		            		+ "'"+ APELLIDO1 + "',"
		            		+ "'"+ APELLIDO2 + "',"
		            		+ "'"+ DIRECCION + "',"
		            		+ "'"+ TELEFONO + "',"
		            		+ "'"+ CUENTACORRIENTE + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		 }
		
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
	 public void deleteRecord(String table_name, String ID) {
	        try {
	            String Query = "DELETE FROM " + table_name + " WHERE ID = '" + ID + "'";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	        }
	    }
	 	//consulta de clientes 
	 public void queryClientes(){
		 String table_name="CLIENTES";
		 String object[][];
	     try {
	            String Query = "SELECT * FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	            
	            String Object[][]={null, null, null, null, null, null, null};

	            while (resultSet.next()) {
	               Object[0][0]="ID: " + resultSet.getString("ID");
	               Object[0][1]= "Nombre: " + resultSet.getString("Nombre");
	               Object[0][2]= resultSet.getString("Apellido") ;
	               Object[0][3]="Edad: " + resultSet.getString("Edad") ;
	               Object[0][4]="Sexo: " + resultSet.getString("Sexo");
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
	 }

	/*M�TODO INSERTAR NUEVA RUTINA*/
	 
	 public void insertDataRutina(String ID_RUTINA,int EJ01,int EJ02,int EJ03,int EJ04,int EJ05,int EJ06,int EJ07,int EJ08,String CLIENTE_ID,String ENTRENADOR,String DIAS) {
		 String table_name;
		 table_name="RUTINAS";
		 try {
	            String Query = "INSERT INTO " + table_name + " VALUES("
	            		+ "EJID.NEXTVAL,"
	            		+ "'"+CLIENTE_ID+ "',"
	            		+ "'"+EJ01+ "',"
	            		+ "'"+EJ02+ "',"
	            		+ "'"+EJ03+ "',"
	            		+ "'"+EJ04+ "',"
	            		+ "'"+EJ05+ "',"
	            		+ "'"+EJ06+ "',"
	            		+ "'"+EJ07+ "',"
	            		+ "'"+EJ08+ "',"
	            		+ "'"+CLIENTE_ID+ "',"
	            		+ "'"+ENTRENADOR+ "',"
	            		+ "'"+ DIAS+ "',";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	 }
	 
	

//METODO MAIN

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		 SQL db = new SQL();
	     db.SQLConnection("oracle","root","");
	     db.createTable("jose");
	     db.insertData("jose", "12", "jose", "marin malagon", "31", "H");
	     db.getValues("jose");
	     db.deleteRecord("jose", "12");
	     db.closeConnection();

	}


	public void insertDataRutinas(String ID_RUTINA,String EJ01,String EJ02,String EJ03,String EJ04,String EJ05,String EJ06,String EJ07,String EJ08,String CLIENTE_ID,String ENTRENADOR,String DIAS) {
		 String table_name;
		 table_name="RUTINAS";
		 try {
	            String Query = "INSERT INTO " + table_name + " VALUES("
	            		+ "'"+ID_RUTINA+ "',"
	            		+ "'"+CLIENTE_ID+ "',"
	            		+ "'"+EJ01+ "',"
	            		+ "'"+EJ02+ "',"
	            		+ "'"+EJ03+ "',"
	            		+ "'"+EJ04+ "',"
	            		+ "'"+EJ05+ "',"
	            		+ "'"+EJ06+ "',"
	            		+ "'"+EJ07+ "',"
	            		+ "'"+EJ08+ "',"
	            		+ "'"+CLIENTE_ID+ "',"
	            		+ "'"+ENTRENADOR+ "',"
	            		+ "'"+ DIAS+"')";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	}
	
	
	//consulta rutinas�?�?//
	 public void queryRutinas(){
		 String table_name="RUTINAS";
		 String object[][];
	     try {
	            String Query = "SELECT ID_RUTINA,EJ1,EJ2,EJ3,EJ4,EJ5,EJ6,EJ7,EJ8, CLIENTE_ID, ENTRENADOR, DIAS FROM RUTINAS" + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	            
	            String Object[][]=	{null, null, null, null, null, null, null, null, null, null, null, null};

	            while (resultSet.next()) {
	               Object[0][0]="ID Rutina: " + resultSet.getString("ID_RUTINA");
	               Object[0][1]= "Ej 1: " + resultSet.getString("EJ1");
	               Object[0][2]= "Ej 2: "+resultSet.getString("EJ2") ;
	               Object[0][3]="Ej 3: " + resultSet.getString("EJ3") ;
	               Object[0][4]="Ej 4: " + resultSet.getString("EJ4");
	               Object[0][5]="Ej 5: " + resultSet.getString("EJ5");
	               Object[0][6]="Ej 6: " + resultSet.getString("EJ6");
	               Object[0][7]="Ej 7: " + resultSet.getString("EJ7");
	               Object[0][8]="Ej 8: " + resultSet.getString("EJ8");
	               Object[0][9]="ID Cliente: " + resultSet.getString("CLIENTE_ID");
	               Object[0][10]="ID Entrenador: " + resultSet.getString("ENTRENADOR");
	               Object[0][11]="Dia: " + resultSet.getString("DIAS");
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
	 }
	//Consulta ejercicios�?�?//
	 public void queryEjercicios(){
		 String table_name="EJERCICIOS";
		 String object[][];
	     try {
	            String Query = "SELECT COD_EJERCICIO,TIEMPO,NOMBRE,PESO,REPETICIONES,DESC_GRAFICA,PUNTOS FROM EJERCICIOS" + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	            
	            String Object[][]=	{null, null, null, null, null, null, null, null, null, null, null, null};

	            while (resultSet.next()) {
	               Object[0][0]="C�digo: " + resultSet.getString("COD_EJERCICIO");
	               Object[0][1]= "Tiempo: " + resultSet.getString("TIEMPO");
	               Object[0][2]= "Nombre: "+resultSet.getString("NOMBRE") ;
	               Object[0][3]="Peso: " + resultSet.getString("PESO") ;
	               Object[0][4]="Repeticiones: " + resultSet.getString("REPETICIONES");
	               Object[0][5]="GIF: " + resultSet.getString("DESC_GRAFICA");
	               Object[0][6]="Puntos: " + resultSet.getString("PUNTOS");
	        
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
	 }
	 
	 
	
	/**public void edittuplaRUTINAS(String ID_RUTINA, int EJ01,int EJ02,int EJ03,int EJ04,int EJ05,int EJ06,int EJ07,int EJ08,String CLIENTE_ID, String ENTRENADOR, String DIAS){
		 try {
			 
			String table_name="RUTINAS";
	           String Query ="UPDATE " + table_name + " SET " +  + "=" + "'" +  + "'"+ " where  = " + ID_RUTINA;
	           Statement st = Conexion.createStatement();
	           st.executeUpdate(Query);
	           JOptionPane.showMessageDialog(null, "Tupla editada");

	       } catch (SQLException ex) {
	           System.out.println(ex.getMessage());
	           JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
	       }
	}*/
}

	 

