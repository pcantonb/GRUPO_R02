package WIP_PaulaCB;

import java.awt.BorderLayout;
import WIP_Gabriel.SQL;
import vistas.Clientes;
import vistas.Entrenador;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;



public class Rutinas extends JFrame {

	private JPanel contentPane;
	private JTextField txtId;
	private JTextField txtColumn;
	private JTextField txtNuevo;
	private JTextField txtEjercicio;
	private JTextField txtEjercicio_1;
	private JTextField txtEjercicio_2;
	private JTextField txtEjercicio_3;
	private JTextField txtEjercicio_4;
	private JTextField txtEjercicio_5;
	private JTextField txtEjercicio_6;
	private JTextField txtEjercicio_7;
	private JTextField txtIdEntrenador;
	private JTextField txtIdCliente;
	private JTextField txtIdRutina;
	private JTextField txtDia;

	/**
	 * Launch the application.
	 */
	/**public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Rutinas frame = new Rutinas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Rutinas() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(88, 88, 250));
		panel.setBounds(0, 0, 140, 400);
		panel.setLayout(null);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(10, 11, 120, 130);
		panel.add(lblNewLabel);
		lblNewLabel.setBorder((Border) new LineBorder(new Color(0, 0, 0)));
		
		JButton btnMiPerfil = new JButton("Mi Perfil");
		btnMiPerfil.setBounds(25, 163, 89, 23);
		panel.add(btnMiPerfil);
		
		JButton btnClientes = new JButton("Clientes");
		btnClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Clientes Clientes1=new Clientes();
				Clientes1.setVisible(true);
				setVisible(false);
				
			}
		});
		btnClientes.setBounds(10, 218, 120, 23);
		panel.add(btnClientes);
		
		
		
		
		JButton btnEntrenadores = new JButton("Entrenadores");
		
		
		btnEntrenadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Entrenador Entrenador1=new Entrenador();
				Entrenador1.setVisible(true);
				setVisible(false);
			}
		});
		btnEntrenadores.setBounds(10, 248, 120, 23);
		panel.add(btnEntrenadores);
		
		JButton btnEjercicios = new JButton("Ejercicios");
		btnEjercicios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Ejercicios Ejercicios1=new Ejercicios();
				Ejercicios1.setVisible(true);
				setVisible(false);
			}
		});
		btnEjercicios.setBounds(10, 278, 120, 23);
		panel.add(btnEjercicios);
		
		JButton btnRutinas = new JButton("Rutinas");
		btnRutinas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Rutinas Rutinas1=new Rutinas();
				Rutinas1.setVisible(true);
			}
		});
		btnRutinas.setBounds(10, 307, 120, 23);
		panel.add(btnRutinas);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(88, 88, 250));
		panel_1.setBounds(140, 50, 634, 10);
		contentPane.add(panel_1);
		
	btnEjercicios.setBounds(10, 278, 120, 23);
	panel.add(btnEjercicios);
	
	JButton btnRutinas1 = new JButton("Rutinas");
	btnRutinas.setBounds(10, 307, 120, 23);
	panel.add(btnRutinas);
	
	JPanel panel_11 = new JPanel();
	panel_1.setBackground(new Color(88, 88, 250));
	panel_1.setBounds(140, 50, 634, 10);
	contentPane.add(panel_1);
	
	JPanel panel_2 = new JPanel();
	panel_2.setBackground(new Color(252, 252, 94));
	panel_2.setBounds(140, 71, 634, 490);
	contentPane.add(panel_2);
	
	JLabel lblRutinas = new JLabel("Rutinas");
	
	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	GroupLayout gl_panel_2 = new GroupLayout(panel_2);
	gl_panel_2.setHorizontalGroup(
		gl_panel_2.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel_2.createSequentialGroup()
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
					.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 604, GroupLayout.PREFERRED_SIZE)
					.addComponent(lblRutinas, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
				.addContainerGap())
	);
	gl_panel_2.setVerticalGroup(
		gl_panel_2.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel_2.createSequentialGroup()
				.addGap(18)
				.addComponent(lblRutinas)
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 416, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(30, Short.MAX_VALUE))
	);
	
	JPanel panel_3 = new JPanel();
	tabbedPane.addTab("Cliente1", null, panel_3, null);
	
	JComboBox comboBox = new JComboBox();
	comboBox.setModel(new DefaultComboBoxModel(new String[] {"Lunes", "Martes", "Mi\u00E9rcoles", "Jueves", "Viernes", "S\u00E1bado"}));
	
	JLabel lblSeleccioneDa = new JLabel("Seleccione D\u00EDa");
	
	JButton btnNewButton = new JButton("Editar Rutina");
	
	
	JButton btnAgregarRutinas = new JButton("Agregar Rutinas");
	btnAgregarRutinas.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			
			SQL conectar=new SQL();
			try {
				conectar.SQLConnection("javaconnect", "root","");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			conectar.insertDataRutinas2( txtEjercicio.getText(), txtEjercicio_1.getText(), txtEjercicio_2.getText(), txtEjercicio_3.getText(), txtEjercicio_4.getText(), txtEjercicio_5.getText(), txtEjercicio_6.getText(), txtEjercicio_7.getText(), txtIdCliente.getText(), txtIdEntrenador.getText(), txtDia.getText());
			
			
			conectar.closeConnection();
			
			
		}
	});
	
	txtColumn = new JTextField();
	txtColumn.setText("COLUMN");
	txtColumn.setColumns(10);
	
	txtNuevo = new JTextField();
	txtNuevo.setText("NUEVO");
	txtNuevo.setColumns(10);
	
	txtEjercicio = new JTextField();
	txtEjercicio.setText("ejercicio 1");
	txtEjercicio.setColumns(10);
	
	txtEjercicio_1 = new JTextField();
	txtEjercicio_1.setText("ejercicio 2");
	txtEjercicio_1.setColumns(10);
	
	txtEjercicio_2 = new JTextField();
	txtEjercicio_2.setText("ejercicio 3");
	txtEjercicio_2.setColumns(10);
	
	txtEjercicio_3 = new JTextField();
	txtEjercicio_3.setText("ejercicio 4");
	txtEjercicio_3.setColumns(10);
	
	txtEjercicio_4 = new JTextField();
	txtEjercicio_4.setText("ejercicio 5");
	txtEjercicio_4.setColumns(10);
	
	txtEjercicio_5 = new JTextField();
	txtEjercicio_5.setText("ejercicio 6");
	txtEjercicio_5.setColumns(10);
	
	txtEjercicio_6 = new JTextField();
	txtEjercicio_6.setText("ejercicio 7");
	txtEjercicio_6.setColumns(10);
	
	txtEjercicio_7 = new JTextField();
	txtEjercicio_7.setText("ejercicio 8");
	txtEjercicio_7.setColumns(10);
	
	txtIdEntrenador = new JTextField();
	txtIdEntrenador.setText("ID Entrenador");
	txtIdEntrenador.setColumns(10);
	
	txtIdCliente = new JTextField();
	txtIdCliente.setText("ID Cliente");
	txtIdCliente.setColumns(10);
	
	txtIdRutina = new JTextField();
	txtIdRutina.setText("ID Rutina");
	txtIdRutina.setColumns(10);
	
	txtDia = new JTextField();
	txtDia.setText("Dia");
	txtDia.setColumns(10);
	
	txtId = new JTextField();
	txtId.setText("ID");
	txtId.setColumns(10);
	GroupLayout gl_panel_3 = new GroupLayout(panel_3);
	gl_panel_3.setHorizontalGroup(
		gl_panel_3.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel_3.createSequentialGroup()
				.addGap(243)
				.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
					.addComponent(lblSeleccioneDa)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
				.addGap(412))
			.addGroup(Alignment.TRAILING, gl_panel_3.createSequentialGroup()
				.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_3.createSequentialGroup()
						.addGap(64)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_panel_3.createSequentialGroup()
								.addGroup(gl_panel_3.createParallelGroup(Alignment.TRAILING)
									.addComponent(txtIdRutina, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(txtIdEntrenador, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(6)
								.addComponent(txtEjercicio_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGroup(gl_panel_3.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED, 92, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtEjercicio_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED))))
					.addGroup(Alignment.TRAILING, gl_panel_3.createSequentialGroup()
						.addContainerGap()
						.addComponent(txtId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(19)))
				.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_3.createSequentialGroup()
						.addComponent(txtIdCliente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(txtNuevo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(txtEjercicio_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_3.createSequentialGroup()
						.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.TRAILING, false)
							.addGroup(gl_panel_3.createSequentialGroup()
								.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
									.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
									.addGroup(gl_panel_3.createSequentialGroup()
										.addGroup(gl_panel_3.createParallelGroup(Alignment.TRAILING)
											.addGroup(gl_panel_3.createSequentialGroup()
												.addComponent(txtDia, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addGap(16))
											.addGroup(gl_panel_3.createSequentialGroup()
												.addComponent(txtEjercicio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.UNRELATED)))
										.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
											.addComponent(txtEjercicio_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addComponent(txtColumn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
								.addGap(29))
							.addGroup(gl_panel_3.createSequentialGroup()
								.addComponent(txtEjercicio_6, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
								.addGap(58)))
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
							.addComponent(btnAgregarRutinas, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
							.addComponent(txtEjercicio_7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
				.addGap(16)
				.addComponent(txtEjercicio_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGap(58))
	);
	gl_panel_3.setVerticalGroup(
		gl_panel_3.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel_3.createSequentialGroup()
				.addContainerGap()
				.addComponent(lblSeleccioneDa)
				.addGap(1)
				.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGap(139)
				.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_3.createSequentialGroup()
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_3.createSequentialGroup()
								.addGap(4)
								.addComponent(txtNuevo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(txtEjercicio_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
								.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
									.addComponent(txtColumn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(txtEjercicio_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGroup(gl_panel_3.createSequentialGroup()
								.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
									.addComponent(txtIdRutina, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(txtIdCliente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
									.addComponent(txtIdEntrenador, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(txtDia, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(txtEjercicio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_3.createSequentialGroup()
								.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel_3.createSequentialGroup()
										.addComponent(txtEjercicio_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
											.addGroup(gl_panel_3.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
												.addComponent(txtEjercicio_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
											.addGroup(gl_panel_3.createSequentialGroup()
												.addGap(18)
												.addComponent(txtId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
									.addGroup(gl_panel_3.createSequentialGroup()
										.addGap(14)
										.addComponent(txtEjercicio_6, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panel_3.createParallelGroup(Alignment.TRAILING)
									.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
									.addGroup(gl_panel_3.createSequentialGroup()
										.addComponent(btnAgregarRutinas, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
										.addGap(3))))
							.addGroup(gl_panel_3.createSequentialGroup()
								.addComponent(txtEjercicio_7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, 82, GroupLayout.PREFERRED_SIZE))))
					.addGroup(gl_panel_3.createSequentialGroup()
						.addComponent(txtEjercicio_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 169, GroupLayout.PREFERRED_SIZE)))
				.addGap(53))
	);
	panel_3.setLayout(gl_panel_3);
	
	
	
	JTextField textField = new JTextField();
	textField.setText("Search...");
	textField.setColumns(10);
	textField.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
	textField.setBounds(688, 19, 86, 20);
	contentPane.add(textField);
	
	JLabel lblBienvenidoATraining = new JLabel("Bienvenido a TRAINING RANK");
	lblBienvenidoATraining.setMaximumSize(new Dimension(200, 14));
	lblBienvenidoATraining.setForeground(new Color(88, 88, 250));
	lblBienvenidoATraining.setFont(new Font("Tahoma", Font.PLAIN, 18));
	lblBienvenidoATraining.setBounds(150, 17, 266, 22);
	contentPane.add(lblBienvenidoATraining);
	
	JScrollPane scrollPane = new JScrollPane();
	
	JLabel lblEjercicios = new JLabel("Ejercicios");
	
	JButton btnAgregarEjercicio = new JButton("Editar Tabla");
		
	btnNewButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			
			
			SQL conectar=new SQL();
			try {
				conectar.SQLConnection("javaconnect", "root","");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			conectar.edittuplaRUTINAS(txtId.getText(),txtColumn.getText(),txtNuevo.getText());
			
			
			conectar.closeConnection();
		}
	});
	
	
	}
	

}
