package WIP_PaulaCB;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

public class Ejercicios extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicios frame = new Ejercicios();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicios() {
		JFrame frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(88, 88, 250));
		panel.setBounds(0, 0, 140, 400);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(10, 11, 120, 130);
		panel.add(lblNewLabel);
		lblNewLabel.setBorder((Border) new LineBorder(new Color(0, 0, 0)));
		
		JButton btnMiPerfil = new JButton("Mi Perfil");
		btnMiPerfil.setBounds(25, 163, 89, 23);
		panel.add(btnMiPerfil);
		
		JButton btnClientes = new JButton("Clientes");
		btnClientes.setBounds(10, 218, 120, 23);
		panel.add(btnClientes);
		
		JButton btnEntrenadores = new JButton("Entrenadores");
		btnEntrenadores.setBounds(10, 248, 120, 23);
		panel.add(btnEntrenadores);
		
		JButton btnEjercicios = new JButton("Ejercicios");
		btnEjercicios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEjercicios.setBounds(10, 278, 120, 23);
		panel.add(btnEjercicios);
		
		JButton btnRutinas = new JButton("Rutinas");
		btnRutinas.setBounds(10, 307, 120, 23);
		panel.add(btnRutinas);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(88, 88, 250));
		panel_1.setBounds(140, 50, 634, 10);
		frame.getContentPane().add(panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(252, 252, 94));
		panel_2.setBounds(140, 71, 634, 490);
		frame.getContentPane().add(panel_2);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel lblEjercicios = new JLabel("Ejercicios");
		
		JButton btnAgregarEjercicio = new JButton("Editar Tabla");
		btnAgregarEjercicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JTextField txtPeso = new JTextField();
		txtPeso.setText("Peso");
		txtPeso.setColumns(10);
		
		JTextField txtRepeticiones = new JTextField();
		txtRepeticiones.setText("Repeticiones");
		txtRepeticiones.setColumns(10);
		
		JTextField txtTiempo = new JTextField();
		txtTiempo.setText("Tiempo");
		txtTiempo.setColumns(10);
		
		JTextField txtGif = new JTextField();
		txtGif.setText("GIF");
		txtGif.setColumns(10);
		
		JTextField txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setColumns(10);
		
		JTextField txtDescripcin = new JTextField();
		txtDescripcin.setText("Descripci\u00F3n");
		txtDescripcin.setColumns(10);
		
		JButton btnAgregarNuevoEjercicio = new JButton("Agregar nuevo ejercicio");
		
		JLabel lblAgregarNuevoEjercicio = new JLabel("Agregar nuevo ejercicio");
		lblAgregarNuevoEjercicio.setForeground(new Color(88, 88, 250));
		lblAgregarNuevoEjercicio.setFont(new Font("Tahoma", Font.PLAIN, 24));
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, gl_panel_2.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 614, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, gl_panel_2.createSequentialGroup()
							.addGap(24)
							.addComponent(lblEjercicios, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
						.addGroup(Alignment.TRAILING, gl_panel_2.createSequentialGroup()
							.addGap(60)
							.addComponent(lblAgregarNuevoEjercicio)
							.addPreferredGap(ComponentPlacement.RELATED, 224, Short.MAX_VALUE)
							.addComponent(btnAgregarEjercicio))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(28)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING, false)
								.addComponent(txtNombre, GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE)
								.addComponent(txtDescripcin)
								.addComponent(txtPeso)
								.addComponent(txtRepeticiones)
								.addComponent(txtTiempo)
								.addComponent(txtGif))
							.addPreferredGap(ComponentPlacement.RELATED, 85, Short.MAX_VALUE)
							.addComponent(btnAgregarNuevoEjercicio, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(4)
					.addComponent(lblEjercicios, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnAgregarEjercicio)
						.addComponent(lblAgregarNuevoEjercicio))
					.addGap(16)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(txtDescripcin, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtPeso, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(txtRepeticiones, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtTiempo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtGif, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(btnAgregarNuevoEjercicio, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(48, Short.MAX_VALUE))
		);
		
		JTable table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"1", "Abdominales", "...", "", "30", "18", null},
			},
			new String[] {
				"C\u00F3digo", "Nombre", "Descripci\u00F3n", "Peso", "Repeticiones", "Tiempo", "GIF"
			}
		));
		scrollPane.setViewportView(table);
		panel_2.setLayout(gl_panel_2);
		
		JLabel label = new JLabel("Bienvenido a TRAINING RANK");
		label.setMaximumSize(new Dimension(200, 14));
		label.setForeground(new Color(88, 88, 250));
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label.setBounds(150, 17, 266, 22);
		frame.getContentPane().add(label);
		
		JTextField textField = new JTextField();
		textField.setText("Search...");
		textField.setColumns(10);
		textField.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		textField.setBounds(688, 19, 86, 20);
		frame.getContentPane().add(textField);
	}
	public JTable getTable() {
		return getTable();
	}

}
