package WIP_Gabriel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

import vistas.Ejercicios;
import vistas.LogIn;
import vistas.Menu;

public class SQL {

	
	private static Connection Conexion = null;
	private String[][]clientes=new String[1][7];


	//M�TODO PERSONALIZADO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
         
	
	//M�TODO QUE FINALIZA LA CONEXION A SQL
	public void closeConnection() {
	        try {
	            Conexion.close();
	            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	           
	            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
	        }
	    }
    
	
	
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
	 public void createTable(String name) {
	        try {
	            String Query = "CREATE TABLE " + name + ""
	                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
	                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	 
     //METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
	 public void insertData(String table_name, String ID, String name, String lastname, String age, String gender) {
	        try {
	            String Query = "INSERT INTO " + table_name + " VALUES("
	            		+ "'"+ ID + "',"
	            		+ "'"+ name + "',"
	            		+ "'"+ lastname + "',"
	            		+ "'"+ age + "',"
	            		+ "'"+ gender + "')";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	    }	
	
	//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
	 public void getValues(String table_name) {
	        try {
	            String Query = "SELECT * FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println("ID: " + resultSet.getString("ID") + " "
	                        + "Nombre: " + resultSet.getString("Nombre") + " " 
							+ resultSet.getString("Apellido") + " "
	                        + "Edad: " + resultSet.getString("Edad") + " "
	                        + "Sexo: " + resultSet.getString("Sexo"));
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
	    }	
		
	    //METODO QUE INSERTA VALORES EN TABLA ENTRENADORES
	 public void insertDataEntrenadores( String DNI,String NOMBRE, String APELLIDO1, String APELLIDO2,String DIRECCION,String TELEFONO,String CUENTACORRIENTE) {
		 String table_name;
		 table_name="ENTRENADOR";
		 try {
	            String Query = "INSERT INTO " + table_name + " VALUES("
	            		+ "'"+ DNI + "',"
	            		+ "'"+ NOMBRE + "',"
	            		+ "'"+ APELLIDO1 + "',"
	            		+ "'"+ APELLIDO2 + "',"
	            		+ "'"+ DIRECCION + "',"
	            		+ "'"+ TELEFONO + "',"
	            		+ "'"+ CUENTACORRIENTE + "')";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	 }
		 //METODO QUE INSERTA VALORES EN TABLA CLIENTES
		 public void insertDataClientes(String DNI,String NOMBRE, String APELLIDO1, String APELLIDO2,String DIRECCION,String TELEFONO,String CUENTACORRIENTE) {
			 String table_name;
			 table_name="CLIENTE";
			 try {
		            String Query = "INSERT INTO " + table_name + " VALUES("
		            		+ "'"+ DNI + "',"
		            		+ "'"+ NOMBRE + "',"
		            		+ "'"+ APELLIDO1 + "',"
		            		+ "'"+ APELLIDO2 + "',"
		            		+ "'"+ DIRECCION + "',"
		            		+ "'"+ TELEFONO + "',"
		            		+ "'"+ CUENTACORRIENTE + "')";
		             Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		 }
		 
		 //METODO PARA AGREGAR EJERCICIOS
		 public void insertDataEjercicios( String TIEMPO, String NOMBRE, String PESO,String REPETICIONES,String DESCRIPCION,String PUNTOS) {
			 String table_name;
			 table_name="EJERCICIOS";
			 
			 try {
				 	
		            String Query = "INSERT INTO " + table_name + " VALUES("
		            		+ "EJID.NEXTVAL,"
		            		+ "'"+ TIEMPO + "',"
		            		+ "'"+ NOMBRE + "',"
		            		+ "'"+ PESO + "',"
		            		+ "'"+ REPETICIONES + "',"
		            		+ "'"+ DESCRIPCION + "',"
		            		+ "'"+ PUNTOS + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		 }
		
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
	 public void deleteRecord(String table_name, String ID) {
	        try {
	            String Query = "DELETE FROM " + table_name + " WHERE ID = '" + ID + "'";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	        }
	    }
	 	//consulta de clientes 
	 public void queryClientes(){
		 String table_name="CLIENTES";
		 String object[][];
	     try {
	            String Query = "SELECT * FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	            
	            String Object[][]={null, null, null, null, null, null, null};

	            while (resultSet.next()) {
	               Object[0][0]="ID: " + resultSet.getString("ID");
	               Object[0][1]= "Nombre: " + resultSet.getString("Nombre");
	               Object[0][2]= resultSet.getString("Apellido") ;
	               Object[0][3]="Edad: " + resultSet.getString("Edad") ;
	               Object[0][4]="Sexo: " + resultSet.getString("Sexo");
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
	 }
	 	//METODO PARA EDITAR TABLAS 
	 public void editarDataEjercicios(Object column,String cambio,String nombre_id) {
		   String table_name="EJERCICIOS";
		   
		   
		   
		   
		   try {
		     
		             String Query = "UPDATE " + table_name + " SET "+column+"="+ "'"+ cambio + "'" +"WHERE"+column+"="+ "'"+ nombre_id + "'";
		               
		               
		             Statement st = Conexion.createStatement();
		             st.executeUpdate(Query);
		             JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		         } catch (SQLException ex) {
		             JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		         }
		  
		  }


   

public void Actualizar( String TIEMPO, String NOMBRE, String PESO, 
                      String REPETICIONES, String DESCRIPCION, String PUNTOS,String COD_EJERCICIO){

try {

    
    
    String Query = "UPDATE EJERCICIOS SET  TIEMPO=?, NOMBRE=?, PESO=?, REPETICIONES=?, DESCRIPCION=? ,PUNTOS=?"
                + "WHERE COD_EJERCICIO=?";
    
  
    
    Statement st = Conexion.createStatement();
    st.executeUpdate(Query);
    JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
} catch (SQLException ex) {
    JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
}
	 
	

}
//METODO QUE EDITA VALORES DEL STIPO INT DE NUESTRA BASE DE DATOS

	 public void edittupla( String COLUMN_NAME, String campo, String nuevo_valor ){
		 try {
			 
			String table_name="EJERCICIOS";
			String Query ="UPDATE " + table_name + " SET " + campo + "=" + "'" + nuevo_valor + "'"+ " where COD_EJERCICIO = " + COLUMN_NAME;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Tupla editada");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
	        }
	 }



//METODO EDITAR PARA RUTINAS

public void edittuplaRUTINAS( String ID_RUTINA, String campo, String nuevo_valor ){
	 try {
		 
		String table_name="RUTINAS";
           String Query ="UPDATE " + table_name + " SET " + campo + "=" + "'" + nuevo_valor + "'"+ " where ID_RUTINA = " + ID_RUTINA;
           Statement st = Conexion.createStatement();
           st.executeUpdate(Query);
           JOptionPane.showMessageDialog(null, "Tupla editada");

       } catch (SQLException ex) {
           System.out.println(ex.getMessage());
           JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
       }
}

public void getValues1(String table_name) {
	   try {
	            String Query = "SELECT COD_EJERCICIO,TIEMPO,NOMBRE,PESO,REPETICIONES,DESC_GRAFICA,PUNTOS FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println(
	                        "COD_EJERCICIO: " + resultSet.getString("COD_EJERCICIO") + " " 
	                        + "TIEMPO: " + resultSet.getString("TIEMPO") + " "
	                        + "NOMBRE: " + resultSet.getString("NOMBRE") + " "
	                        + "PESO: " + resultSet.getString("PESO") + " "
	                        + "REPETICIONES: " + resultSet.getString("REPETICIONES") + " "
	                        + "DESC_GRAFICA: " + resultSet.getString("DESC_GRAFICA") + " "
	                        + "PUNTOS: " + resultSet.getString("PUNTOS") + " ");
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
	        }
	}

public void getValuesClientes(String table_name) {
	   try {
	            String Query = "SELECT DNI,NOMBRE,APELLDIO1,APELLIDO2,DIRECCION,TELEFONO,CUENTA_CORRIENTE FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println(
	                        "DNI: " + resultSet.getString("DNI") + " " 
	                        + "NOMBRE: " + resultSet.getString("NOMBRE") + " "
	                        + "APELLDIO1: " + resultSet.getString("APELLDIO1") + " "
	                        + "APELLIDO2: " + resultSet.getString("APELLIDO2") + " "
	                        + "DIRECCION: " + resultSet.getString("DIRECCION") + " "
	                        + "TELEFONO: " + resultSet.getString("TELEFONO") + " "
	                        + "CUENTA_CORRIENTE: " + resultSet.getString("CUENTA_CORRIENTE") + " ");
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
	        }
	}
public void getValuesClientesUnico(String DNI) {
	   try {
	            String Query = "SELECT DNI,NOMBRE,APELLDIO1,APELLIDO2,DIRECCION,TELEFONO,CUENTA_CORRIENTE FROM CLIENTES WHERE "+DNI ;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println(
	                        "DNI: " + resultSet.getString("DNI") + " " 
	                        + "NOMBRE: " + resultSet.getString("NOMBRE") + " "
	                        + "APELLDIO1: " + resultSet.getString("APELLDIO1") + " "
	                        + "APELLIDO2: " + resultSet.getString("APELLIDO2") + " "
	                        + "DIRECCION: " + resultSet.getString("DIRECCION") + " "
	                        + "TELEFONO: " + resultSet.getString("TELEFONO") + " "
	                        + "CUENTA_CORRIENTE: " + resultSet.getString("CUENTA_CORRIENTE") + " ");
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
	        }
	}

public void getValuesEntrenadoresUnico(String DNI) {
	   try {
	            String Query = "SELECT DNI,NOMBRE,APELLDIO1,APELLIDO2,DIRECCION,TELEFONO,CUENTA_CORRIENTE FROM ENTRENADORES WHERE "+DNI ;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println(
	                        "DNI: " + resultSet.getString("DNI") + " " 
	                        + "NOMBRE: " + resultSet.getString("NOMBRE") + " "
	                        + "APELLDIO1: " + resultSet.getString("APELLDIO1") + " "
	                        + "APELLIDO2: " + resultSet.getString("APELLIDO2") + " "
	                        + "DIRECCION: " + resultSet.getString("DIRECCION") + " "
	                        + "TELEFONO: " + resultSet.getString("TELEFONO") + " "
	                        + "CUENTA_CORRIENTE: " + resultSet.getString("CUENTA_CORRIENTE") + " ");
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
	        }
	}


public void insertDataRutinas(String DIAS) {
	 String table_name;
	 table_name="RUTINAS";
	 try {
           String Query = "INSERT INTO " + table_name + " VALUES("
        	    + "RUT.NEXTVAL,"
           		+ "'"+ DIAS+ "',";
           Statement st = Conexion.createStatement();
           st.executeUpdate(Query);
           JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
       }
}

public void insertDataRutinas2( String EJ01,String EJ02,String EJ03,String EJ04,String EJ05,String EJ06,String EJ07,String EJ08,String CLIENTE_ID, String ENTRENADOR, String DIAS) {
	 String table_name;
	 table_name="RUTINAS";
	 try {
          String Query = "INSERT INTO " + table_name + " VALUES("
       	    + "RUT.NEXTVAL,"+
       	     "'"+ EJ01+ "',"
       	    + "'"+ EJ02+ "',"
       	    + "'"+ EJ03+ "',"
       	    + "'"+ EJ04+ "',"
       	    + "'"+ EJ05+ "',"
       	    + "'"+ EJ06+ "',"
       	    + "'"+ EJ07+ "',"
       	    + "'"+ EJ08+ "',"
       	    + "'"+ CLIENTE_ID+ "',"
       	    + "'"+ ENTRENADOR+ "',"
       	    + "'"+ DIAS+"')";
          Statement st = Conexion.createStatement();
          st.executeUpdate(Query);
          JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
      } catch (SQLException ex) {
          JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
      }
}
public String [][] getValuesEntrenadoresUnico1( ) {
	   try {
	            String Query = "SELECT * FROM CLIENTE WHERE DNI= '12345678' " ;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	            String[][]clientes =new String[2][7];
	            	clientes[1][0]=resultSet.getString("DNI");
	            	clientes[1][1]=resultSet.getString("NOMBRE");
	            	clientes[1][2]=resultSet.getString("APELLDIO1");
	            	clientes[1][3]=resultSet.getString("APELLIDO2");
	            	clientes[1][4]=resultSet.getString("DIRECCION");
	            	clientes[1][5]=resultSet.getString("TELEFONO");
	            	clientes[1][6]=resultSet.getString("CUENTA_CORRIENTE");
	            	 

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
	            
	        }
	return (clientes);
	   
	}
public String [][] getValuesEntrenadoresUnico2(String DNI ) {
	
	String clientes[][]=new String[1][7];
	clientes[0][0]="DNI";
	clientes[0][1]="NOMBRE";
	clientes[0][2]="APELLIDO1";
	clientes[0][2]="APELLIDO2";
	clientes[0][2]="DIRECCION";
	clientes[0][2]="TELEFONO";
	clientes[0][2]="CUENTA_CORRIENTE";
	
	for (int i=1;i<clientes.length;i++){
		for(int j=0;j<7;j++){
			clientes[i][j]="";
		}
		
	}
		int i=1;
	   try {
	            String Query = "SELECT DNI,NOMBRE,APELLIDO1,APELLIDO2,DIRECCION,TELEFONO,CUENTA_CORRIENTE FROM CLIENTE WHERE DNI="+"'"+DNI+"'" ;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	            while(resultSet.next()){
	           
	            	clientes[i][0]=resultSet.getString("DNI");
	            	clientes[i][1]=resultSet.getString("NOMBRE");
	            	clientes[i][2]=resultSet.getString("APELLDIO1");
	            	clientes[i][3]=resultSet.getString("APELLIDO2");
	            	clientes[i][4]=resultSet.getString("DIRECCION");
	            	clientes[i][5]=resultSet.getString("TELEFONO");
	            	clientes[i][6]=resultSet.getString("CUENTA_CORRIENTE");
	            	i++;
	            	 
	            }
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
	            
	        }
	return (clientes);
	   
	}

//METODO QUE OBTIENE ID DE NUESTRA BASE DE DATOS		
	 public String getID(String DNI) {
		 //Vector v01=new Vector(10);
		 String a = null;
	        try {
	            String Query = "SELECT * FROM CLIENTE WHERE DNI='"+DNI+"'";
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                //System.out.println("DNI: " + resultSet.getString("DNI"));
	                a=resultSet.getString("DNI");
	               
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
			return a;
	    }	


	 
	 ///////////////////////////////////////////////////////////7
	 
	 public String [][] getVal(String DNI) {
		   try {
		            String Query = "SELECT * FROM CLIENTE WHERE DNI= '"+DNI+"' " ;
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		            String[][]clientes =new String[2][7];
		            	clientes[1][0]=resultSet.getString("DNI");
		            	//clientes[1][1]=resultSet.getString("NOMBRE");
		            	//clientes[1][2]=resultSet.getString("APELLDIO1");
		            	//clientes[1][3]=resultSet.getString("APELLIDO2");
		            	//clientes[1][4]=resultSet.getString("DIRECCION");
		            	//clientes[1][5]=resultSet.getString("TELEFONO");
		            	//clientes[1][6]=resultSet.getString("CUENTA_CORRIENTE");
		            	 

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
		            
		        }
		return (clientes);
		   
		}
	 
	 
	 public String getnombre(String DNI) {
		 //Vector v01=new Vector(10);
		 String a = null;
	        try {
	            String Query = "SELECT * FROM CLIENTE WHERE DNI='"+DNI+"'";
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                //System.out.println("DNI: " + resultSet.getString("DNI"));
	                a=resultSet.getString("NOMBRE");
	               
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
			return a;
	    }	
	 public String getidAPELLIDO1(String DNI ) {
			String a=null;
			   try {
			            String Query = "SELECT * FROM CLIENTE WHERE DNI='"+DNI+"'";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			            while(resultSet.next()){
			           a=resultSet.getString("APELLIDO1");
			           
			           
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			            
			        }
			return a;
			   
			}
	 public String getidAPELLIDO2(String DNI ) {
			String a=null;
			   try {
			            String Query = "SELECT * FROM CLIENTE WHERE DNI='"+DNI+"'";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			            while(resultSet.next()){
			           a=resultSet.getString("APELLIDO2");
			           
			           
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			            
			        }
			return a;
			   
			}
		public String getidDIRECCION(String DNI ) {
			String a=null;
			   try {
			            String Query = "SELECT * FROM CLIENTE WHERE DNI='"+DNI+"'";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			            while(resultSet.next()){
			           a=resultSet.getString("DIRECCION");
			           
			           
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			            
			        }
			return a;
			   
			}
		public String getidTELEFONO(String DNI ) {
			String a=null;
			   try {
			            String Query = "SELECT * FROM CLIENTE WHERE DNI='"+DNI+"'";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			            while(resultSet.next()){
			           a=resultSet.getString("TELEFONO");
			           
			           
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			            
			        }
			return a;
			   
			}
		public String getidCUENTA_CORRIENTE(String DNI ) {
			String a=null;
			   try {
			            String Query = "SELECT * FROM CLIENTE WHERE DNI='"+DNI+"'";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			            while(resultSet.next()){
			           a=resultSet.getString("CUENTA_CORRIENTE");
			           
			           
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			            
			        }
			return a;
			   
			}
		///////////////////////////////////////////////////////////////////////////////////////////////
		//METODOS PARA BOTON DE BUSCAR ENTRENADORES
		
		 public String getIDE(String DNI) {
			 //Vector v01=new Vector(10);
			 String a = null;
		        try {
		            String Query = "SELECT * FROM ENTRENADOR WHERE DNI='"+DNI+"'";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

		            while (resultSet.next()) {
		                //System.out.println("DNI: " + resultSet.getString("DNI"));
		                a=resultSet.getString("DNI");
		               
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
				return a;
		    }
		 
		 public String getnombreE(String DNI) {
			 //Vector v01=new Vector(10);
			 String a = null;
		        try {
		            String Query = "SELECT * FROM ENTRENADOR WHERE DNI='"+DNI+"'";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

		            while (resultSet.next()) {
		                //System.out.println("DNI: " + resultSet.getString("DNI"));
		                a=resultSet.getString("NOMBRE");
		               
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
				return a;
		    }	
		 public String getidAPELLIDO1E(String DNI ) {
				String a=null;
				   try {
				            String Query = "SELECT * FROM ENTRENADOR WHERE DNI='"+DNI+"'";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);
				            while(resultSet.next()){
				           a=resultSet.getString("APELLIDO1");
				           
				           
				            }
				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
				            
				        }
				return a;
				   
				}
		 public String getidAPELLIDO2E(String DNI ) {
				String a=null;
				   try {
				            String Query = "SELECT * FROM ENTRENADOR WHERE DNI='"+DNI+"'";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);
				            while(resultSet.next()){
				           a=resultSet.getString("APELLIDO2");
				           
				           
				            }
				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
				            
				        }
				return a;
				   
				}
			public String getidDIRECCIONE(String DNI ) {
				String a=null;
				   try {
				            String Query = "SELECT * FROM ENTRENADOR WHERE DNI='"+DNI+"'";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);
				            while(resultSet.next()){
				           a=resultSet.getString("DIRECCION");
				           
				           
				            }
				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
				            
				        }
				return a;
				   
				}
			public String getidTELEFONOE(String DNI ) {
				String a=null;
				   try {
				            String Query = "SELECT * FROM ENTRENADOR WHERE DNI='"+DNI+"'";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);
				            while(resultSet.next()){
				           a=resultSet.getString("TELEFONO");
				           
				           
				            }
				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
				            
				        }
				return a;
				   
				}
			public String getidCUENTA_CORRIENTEE(String DNI ) {
				String a=null;
				   try {
				            String Query = "SELECT * FROM ENTRENADOR WHERE DNI='"+DNI+"'";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);
				            while(resultSet.next()){
				           a=resultSet.getString("CUENTA_CORRIENTE");
				           
				           
				            }
				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
				            
				        }
				return a;
				   
				}
			 public void insertDataPassClientes(String DNI,String PALABRA_PASS,String ROL) {
				 String table_name;
				 
				 try {
			            String Query = "INSERT INTO LOGIN VALUES("
			            		+ "'"+ DNI + "',"
			            		+ "'"+ PALABRA_PASS + "',"
			            		+ "'"+ ROL + "')";
			             Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			        }
			 }
			 public void insertDataPassEntrenador(String DNI,String PALABRA_PASS,String ROL) {
				 
				 try {
			            String Query = "INSERT INTO LOGIN VALUES("
			            		+ "'"+ DNI + "',"
			            		+ "'"+ PALABRA_PASS + "',"
			            		+ "'"+ ROL + "')";
			             Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			        }
			 }
			 
			//////////////////////////////////////////////////
			//METODO PARA INSERT DATA EN LOGIN
			
			 

			    
			    public String LOGIN (String DNI,String PALABRA_PASS){
			    	
			    	String valor="";
			    	String A="";
			    	String e="";
			    	
			    	
			    	
			    	try {
			    		String Query="SELECT DNI,PALABRA_PASS,ROL FROM LOGIN WHERE DNI='"+DNI+ "' AND PALABRA_PASS ='"+PALABRA_PASS+ "'";
			    		
			    		Statement st =Conexion.createStatement();
			    		java.sql.ResultSet resultSet;
			    		
			    		resultSet=st.executeQuery(Query);
			    		while (resultSet.next()){
			    		A=resultSet.getString("DNI");
			    		e=resultSet.getString("ROL");
			    		}
			    		
			    		 valor=e;
			    	}
			    catch(SQLException ex) {
			    	JOptionPane.showMessageDialog(null,"Error en la adquisicion de datos");
			    }
			    return valor;
			    }

			
			    public String getTIEMPO(String NOMBRE ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM EJERCICIOS WHERE NOMBRE='"+NOMBRE+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("TIEMPO");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
			    public String getNOMBRE(String NOMBRE ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM EJERCICIOS WHERE NOMBRE='"+NOMBRE+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("NOMBRE");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
			    public String getPESO(String NOMBRE ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM EJERCICIOS WHERE NOMBRE='"+NOMBRE+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("PESO");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
			    public String getidREPETICIONES(String NOMBRE ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM EJERCICIOS WHERE NOMBRE='"+NOMBRE+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("REPETICIONES");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
			    public String getDESCRIPCION(String NOMBRE ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM EJERCICIOS WHERE NOMBRE='"+NOMBRE+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("DESC_GRAFICA");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
			    public String getPUNTOS(String NOMBRE ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM EJERCICIOS WHERE NOMBRE='"+NOMBRE+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("PUNTOS");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
			    public String getidEJERCICIO(String NOMBRE ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM EJERCICIOS WHERE NOMBRE='"+NOMBRE+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("COD_EJERCICIO");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
			    public String getidEJ1(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("EJ1");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
			    public String getidDIAS(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("DIAS");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
			    public String getidEJ2(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("EJ2");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
			}
			    public String getidEJ3(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("EJ3");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;}
			    public String getidEJ4(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("EJ4");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;}
			    public String getidEJ5(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("EJ5");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;}
			    public String getidEJ6(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("EJ6");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;}
			    public String getidEJ7(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("EJ7");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;}
			    public String getidEJ8(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("EJ8");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;}
			    
			    public String getidENTRENADOR(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("ENTRENADOR");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;}
			    public String getidCLIENTE(String CLIENTE_ID ) {
					String a=null;
					   try {
					            String Query = "SELECT * FROM RUTINAS WHERE CLIENTE_ID='"+CLIENTE_ID+"'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            while(resultSet.next()){
					           a=resultSet.getString("CLIENTE_ID");
					           
					           
					            }
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
					            
					        }
					return a;
					   
					}
}