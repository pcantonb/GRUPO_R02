package WIP_JoseMagraneFanego;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class SQL {

	
	private static Connection Conexion = null;
	private String[][]clientes =new String[1][7];

	//M�TODO PERSONALIZADO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
         
	
	//M�TODO QUE FINALIZA LA CONEXION A SQL
	public void closeConnection() {
	        try {
	            Conexion.close();
	            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	           
	            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
	        }
	    }
    
	
	
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
	 public void createTable(String name) {
	        try {
	            String Query = "CREATE TABLE " + name + ""
	                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
	                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	 
     //METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
	 public void insertData(String table_name, String ID, String name, String lastname, String age, String gender) {
	        try {
	            String Query = "INSERT INTO " + table_name + " VALUES("
	            		+ "'"+ ID + "',"
	            		+ "'"+ name + "',"
	            		+ "'"+ lastname + "',"
	            		+ "'"+ age + "',"
	            		+ "'"+ gender + "')";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	    }	
	
	//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
	 public void getValues(String table_name) {
	        try {
	            String Query = "SELECT * FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println("ID: " + resultSet.getString("ID") + " "
	                        + "Nombre: " + resultSet.getString("Nombre") + " " 
							+ resultSet.getString("Apellido") + " "
	                        + "Edad: " + resultSet.getString("Edad") + " "
	                        + "Sexo: " + resultSet.getString("Sexo"));
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
	    }	
		
	    //METODO QUE INSERTA VALORES EN TABLA ENTRENADORES
	 public void insertDataEntrenadores( String DNI,String NOMBRE, String APELLIDO1, String APELLIDO2,String DIRECCION,String TELEFONO,String CUENTACORRIENTE) {
		 String table_name;
		 table_name="ENTRENADOR";
		 try {
	            String Query = "INSERT INTO " + table_name + " VALUES("
	            		+ "'"+ DNI + "',"
	            		+ "'"+ NOMBRE + "',"
	            		+ "'"+ APELLIDO1 + "',"
	            		+ "'"+ APELLIDO2 + "',"
	            		+ "'"+ DIRECCION + "',"
	            		+ "'"+ TELEFONO + "',"
	            		+ "'"+ CUENTACORRIENTE + "')";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	 }
		 //METODO QUE INSERTA VALORES EN TABLA CLIENTES
		 public void insertDataClientes( String DNI,String NOMBRE, String APELLIDO1, String APELLIDO2,String DIRECCION,String TELEFONO,String CUENTACORRIENTE) {
			 String table_name;
			 table_name="CLIENTE";
			 try {
		            String Query = "INSERT INTO " + table_name + " VALUES("
		            		+ "'"+ DNI + "',"
		            		+ "'"+ NOMBRE + "',"
		            		+ "'"+ APELLIDO1 + "',"
		            		+ "'"+ APELLIDO2 + "',"
		            		+ "'"+ DIRECCION + "',"
		            		+ "'"+ TELEFONO + "',"
		            		+ "'"+ CUENTACORRIENTE + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		 }
		 
		 //METODO PARA AGREGAR EJERCICIOS
		 public void insertDataEjercicios( String TIEMPO, String NOMBRE, String PESO,String REPETICIONES,String DESCRIPCION,String PUNTOS) {
			 String table_name;
			 table_name="EJERCICIOS";
			 
			 try {
				 	
		            String Query = "INSERT INTO " + table_name + " VALUES("
		            		+ "EJID.NEXTVAL,"
		            		+ "'"+ TIEMPO + "',"
		            		+ "'"+ NOMBRE + "',"
		            		+ "'"+ PESO + "',"
		            		+ "'"+ REPETICIONES + "',"
		            		+ "'"+ DESCRIPCION + "',"
		            		+ "'"+ PUNTOS + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		 }
		
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
	 public void deleteRecord(String table_name, String ID) {
	        try {
	            String Query = "DELETE FROM " + table_name + " WHERE ID = '" + ID + "'";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	        }
	    }
	 	//consulta de clientes 
	 public void queryClientes(){
		 String table_name="CLIENTES";
		 String object[][];
	     try {
	            String Query = "SELECT * FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	            
	            String Object[][]={null, null, null, null, null, null, null};

	            while (resultSet.next()) {
	               Object[0][0]="ID: " + resultSet.getString("ID");
	               Object[0][1]= "Nombre: " + resultSet.getString("Nombre");
	               Object[0][2]= resultSet.getString("Apellido") ;
	               Object[0][3]="Edad: " + resultSet.getString("Edad") ;
	               Object[0][4]="Sexo: " + resultSet.getString("Sexo");
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
	 }
	 public String[][] getValuesEntrenadoresUnico1 (String DNI) {
		   try {
		            String Query = "SELECT DNI,NOMBRE,APELLDIO1,APELLIDO2,DIRECCION,TELEFONO,CUENTA_CORRIENTE FROM ENTRENADORES WHERE "+DNI ;
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		            String[][]clientes =new String[1][7];
		            	clientes[0][0]=resultSet.getString("DNI");
		            	clientes[0][1]=resultSet.getString("NOMBRE");
		            	clientes[0][2]=resultSet.getString("APELLDIO1");
		            	clientes[0][3]=resultSet.getString("APELLIDO2");
		            	clientes[0][4]=resultSet.getString("DIRECCION");
		            	clientes[0][5]=resultSet.getString("TELEFONO");
		            	clientes[0][6]=resultSet.getString("CUENTA_CORRIENTE");
		            	

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
		        }
		return clientes;
		}	 
	
	
	
}