package vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JProgressBar;

public class Ranking extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ranking frame = new Ranking();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ranking() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setTitle("TRAINING RANK");
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 255));
		panel.setBounds(10, 42, 764, 10);
		contentPane.add(panel);
		
		JLabel lblPuntuacion = new JLabel("97");
		lblPuntuacion.setHorizontalAlignment(SwingConstants.CENTER);
		lblPuntuacion.setBounds(388, 63, 101, 14);
		contentPane.add(lblPuntuacion);
		
		JLabel label_1 = new JLabel("90");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setBounds(388, 102, 101, 14);
		contentPane.add(label_1);
		
		JLabel label_4 = new JLabel("77");
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		label_4.setBounds(388, 142, 101, 14);
		contentPane.add(label_4);
		
		JLabel label_8 = new JLabel("44");
		label_8.setHorizontalAlignment(SwingConstants.CENTER);
		label_8.setBounds(388, 198, 101, 14);
		contentPane.add(label_8);
		
		JLabel label_10 = new JLabel("31");
		label_10.setHorizontalAlignment(SwingConstants.CENTER);
		label_10.setBounds(388, 250, 101, 14);
		contentPane.add(label_10);
		
		JLabel label_11 = new JLabel("75");
		label_11.setHorizontalAlignment(SwingConstants.CENTER);
		label_11.setBounds(388, 295, 101, 14);
		contentPane.add(label_11);
		
		JLabel label_14 = new JLabel("90");
		label_14.setHorizontalAlignment(SwingConstants.CENTER);
		label_14.setBounds(388, 335, 101, 14);
		contentPane.add(label_14);
		
		JLabel label_15 = new JLabel("1");
		label_15.setHorizontalAlignment(SwingConstants.CENTER);
		label_15.setBounds(388, 383, 101, 14);
		contentPane.add(label_15);
		
		JLabel label_16 = new JLabel("-1");
		label_16.setHorizontalAlignment(SwingConstants.CENTER);
		label_16.setBounds(388, 438, 101, 14);
		contentPane.add(label_16);
		
		JLabel label_17 = new JLabel("100");
		label_17.setHorizontalAlignment(SwingConstants.CENTER);
		label_17.setBounds(388, 488, 101, 14);
		contentPane.add(label_17);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(null);
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(127, 11, 534, 26);
		contentPane.add(panel_1);
		
		JLabel lblRanking = new JLabel("RANKING");
		lblRanking.setForeground(new Color(88, 88, 225));
		panel_1.add(lblRanking);
		lblRanking.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblRanking.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(252, 252, 54));
		panel_2.setForeground(new Color(60, 179, 113));
		panel_2.setBounds(10, 54, 764, 496);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JProgressBar progressBar_1 = new JProgressBar();
		progressBar_1.setBounds(90, 42, 257, 29);
		progressBar_1.setValue(90);
		
		panel_2.add(progressBar_1);
		
		JProgressBar progressBar_2 = new JProgressBar();
		progressBar_2.setBounds(90, 84, 257, 29);
		progressBar_2.setValue(80);
		panel_2.add(progressBar_2);
		
		JProgressBar progressBar_3 = new JProgressBar();
		progressBar_3.setBounds(90, 136, 257, 29);
		progressBar_3.setValue(44);
		panel_2.add(progressBar_3);
		
		JProgressBar progressBar_4 = new JProgressBar();
		progressBar_4.setBounds(90, 188, 257, 29);
		progressBar_4.setValue(33);
		panel_2.add(progressBar_4);
		
		JProgressBar progressBar_5 = new JProgressBar();
		progressBar_5.setBounds(90, 231, 257, 29);
		progressBar_5.setValue(77);
		panel_2.add(progressBar_5);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(90, 5, 257, 26);
		progressBar.setValue(97);
		panel_2.add(progressBar);
		
		JProgressBar progressBar_6 = new JProgressBar();
		progressBar_6.setBounds(90, 426, 257, 29);
		progressBar_6.setValue(1110);
		panel_2.add(progressBar_6);
		
		JProgressBar progressBar_7 = new JProgressBar();
		progressBar_7.setBounds(90, 383, 257, 29);
		progressBar_7.setValue(-11);
		panel_2.add(progressBar_7);
		
		JProgressBar progressBar_8 = new JProgressBar();
		progressBar_8.setBounds(90, 331, 257, 29);
		progressBar_8.setValue(7);
		panel_2.add(progressBar_8);
		
		JProgressBar progressBar_9 = new JProgressBar();
		progressBar_9.setBounds(90, 279, 257, 29);
		progressBar_9.setValue(90);
		panel_2.add(progressBar_9);
		
		JLabel lblUsuario_6 = new JLabel("Bruce Lee");
		lblUsuario_6.setBounds(0, 279, 80, 14);
		panel_2.add(lblUsuario_6);
		
		JLabel lblUsuario_5 = new JLabel("Demy Moore");
		lblUsuario_5.setBounds(0, 231, 80, 14);
		panel_2.add(lblUsuario_5);
		
		JLabel lblUsuario_4 = new JLabel("Johnny Deep");
		lblUsuario_4.setBounds(0, 188, 80, 14);
		panel_2.add(lblUsuario_4);
		
		JLabel lblUsuario_3 = new JLabel("Pilar Rubio");
		lblUsuario_3.setBounds(0, 136, 80, 14);
		panel_2.add(lblUsuario_3);
		
		JLabel lblUsuario_2 = new JLabel("Mike Tyson");
		lblUsuario_2.setBounds(0, 84, 80, 14);
		panel_2.add(lblUsuario_2);
		
		JLabel lblUsuario_1 = new JLabel("Johnny Bravo");
		lblUsuario_1.setBounds(10, 42, 70, 14);
		panel_2.add(lblUsuario_1);
		
		JLabel lblUsuario = new JLabel("Hulk Hogan");
		lblUsuario.setBounds(10, 5, 70, 14);
		panel_2.add(lblUsuario);
		
		JLabel lblUsuario_7 = new JLabel("Mariano Rajoy");
		lblUsuario_7.setBounds(0, 331, 80, 14);
		panel_2.add(lblUsuario_7);
		
		JLabel lblUsuario_8 = new JLabel("Justin Biever");
		lblUsuario_8.setBounds(10, 383, 70, 14);
		panel_2.add(lblUsuario_8);
		
		JLabel lblUsuario_9 = new JLabel("Chuck Norris");
		lblUsuario_9.setBounds(10, 426, 75, 14);
		panel_2.add(lblUsuario_9);
	}
}
