package vistas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Dimension;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class PERFILCLIENTE extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PERFILCLIENTE frame = new PERFILCLIENTE();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PERFILCLIENTE() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(252, 252, 54));
		panel.setBounds(154, 130, 620, 420);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(88, 88, 225));
		panel_1.setBounds(10, 21, 134, 382);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel label_1 = new JLabel("Imagen");
		label_1.setBorder((Border) new LineBorder(new Color(0, 0, 0)));
		label_1.setBounds(10, 11, 120, 130);
		panel_1.add(label_1);
		
		JButton btnEjercicios = new JButton("Ejercicios");
		btnEjercicios.setBounds(10, 277, 120, 23);
		panel_1.add(btnEjercicios);
		
		JButton btnRutinas = new JButton("Rutinas");
		btnRutinas.setBounds(10, 189, 120, 23);
		panel_1.add(btnRutinas);
		btnRutinas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Rutinas rutina1=new Rutinas();
				rutina1.setVisible(true);
						
			}
		});
		btnEjercicios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Ejercicios ejercicio1=new Ejercicios();
				ejercicio1.setVisible(true);
				
				
			}
		});
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(88, 88, 250));
		panel_2.setBounds(140, 109, 634, 10);
		contentPane.add(panel_2);
		
		JLabel label = new JLabel("Bienvenido a TRAINING RANK");
		label.setMaximumSize(new Dimension(200, 14));
		label.setForeground(new Color(88, 88, 250));
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label.setBounds(151, 46, 266, 22);
		contentPane.add(label);
		
		
		JLabel label_3 = new JLabel("");
		label_3.setBounds(238, 282, 133, 127);
		label_3.setIcon(new ImageIcon(Menu.class.getResource("/documentos/LOGOTIPO/favicon1.jpg")));
		panel.add(label_3);
		
		JButton btnRanking = new JButton("RANKING");
		btnRanking.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Ranking lol=new Ranking();
				lol.setVisible(true);
				setVisible(false);
			}
		});
		btnRanking.setBounds(195, 31, 101, 42);
		panel.add(btnRanking);
		
		JLabel lblAccederAlRanking = new JLabel("Acceder al RANKING");
		lblAccederAlRanking.setMaximumSize(new Dimension(200, 14));
		lblAccederAlRanking.setForeground(new Color(88, 88, 250));
		lblAccederAlRanking.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAccederAlRanking.setBounds(10, 47, 266, 22);
		panel.add(lblAccederAlRanking);
		
		JButton btnC = new JButton("Cuenta");
		btnC.setBounds(10, 107, 89, 23);
		panel.add(btnC);
		
		JButton btnNotificaciones = new JButton("Informaci\u00F3n y ayuda");
		btnNotificaciones.setBounds(10, 154, 167, 49);
		panel.add(btnNotificaciones);
		
		JButton btnNotificaciones_1 = new JButton("Notificaciones");
		btnNotificaciones_1.setBounds(490, 11, 120, 33);
		panel.add(btnNotificaciones_1);
		
		JButton btnNewButton = new JButton("Tu estado");
		btnNewButton.setBounds(10, 227, 167, 60);
		panel.add(btnNewButton);
		
		JButton btnTuPersonalTrainer = new JButton("Tu Personal Trainer");
		btnTuPersonalTrainer.setBounds(10, 306, 167, 60);
		panel.add(btnTuPersonalTrainer);
		
	}
}
