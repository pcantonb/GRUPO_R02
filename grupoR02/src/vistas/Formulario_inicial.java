package vistas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Formulario_inicial extends JFrame {

	private JPanel contentPane;
	private JTextField txtExplicaTuLesion;
	private JTextField txtIndicaTuPeso;
	private JTextField txtIndicaTuEdad;
	private JTextField txtIndicaTuAltura;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Formulario_inicial frame = new Formulario_inicial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Formulario_inicial() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 669, 365);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(10, 0, 653, 326);
		panel_2.setBackground(new Color(252, 252, 54));
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Realizas algun tipo de actividad fisica");
		lblNewLabel.setForeground(new Color(88, 88, 250));
		lblNewLabel.setBounds(40, 85, 180, 14);
		panel_2.add(lblNewLabel);
		
		JLabel lblCuestionarioInicial = new JLabel("Cuestionario inicial");
		lblCuestionarioInicial.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblCuestionarioInicial.setForeground(new Color(88, 88, 250));
		lblCuestionarioInicial.setBounds(223, 11, 180, 22);
		panel_2.add(lblCuestionarioInicial);
		
		JRadioButton chckbxSiTodosLos = new JRadioButton("Si todos los dias ");
		chckbxSiTodosLos.setBackground(new Color(252, 252, 54));
		chckbxSiTodosLos.setBounds(35, 106, 105, 23);
		panel_2.add(chckbxSiTodosLos);
		
		JRadioButton chckbxDosVecesPor = new JRadioButton("Dos Veces por semana  ");
		chckbxDosVecesPor.setBackground(new Color(252, 252, 54));
		chckbxDosVecesPor.setBounds(35, 132, 144, 23);
		panel_2.add(chckbxDosVecesPor);
		
		JRadioButton chckbxNunca = new JRadioButton("Nunca ");
		chckbxNunca.setBackground(new Color(252, 252, 54));
		chckbxNunca.setBounds(35, 158, 105, 23);
		panel_2.add(chckbxNunca);
		
		JRadioButton rdbtnNo = new JRadioButton("No ");
		rdbtnNo.setBackground(new Color(252, 252, 54));
		rdbtnNo.setBounds(259, 132, 144, 23);
		panel_2.add(rdbtnNo);
		
		JRadioButton rdbtnSi = new JRadioButton("Si ");
		rdbtnSi.setBackground(new Color(252, 252, 54));
		rdbtnSi.setBounds(259, 106, 105, 23);
		panel_2.add(rdbtnSi);
		
		JLabel lblHasEstadoApuntado = new JLabel("Has estado apuntado en un gimnasio antes");
		lblHasEstadoApuntado.setForeground(new Color(88, 88, 250));
		lblHasEstadoApuntado.setBounds(264, 85, 217, 14);
		panel_2.add(lblHasEstadoApuntado);
		
		JRadioButton rdbtnNo_1 = new JRadioButton("No");
		rdbtnNo_1.setBackground(new Color(252, 252, 54));
		rdbtnNo_1.setBounds(518, 132, 105, 23);
		panel_2.add(rdbtnNo_1);
		
		JRadioButton rdbtnSi_1 = new JRadioButton("Si ");
		rdbtnSi_1.setBackground(new Color(252, 252, 54));
		rdbtnSi_1.setBounds(518, 106, 37, 23);
		panel_2.add(rdbtnSi_1);
		
		JLabel lblAlgunTipoDe = new JLabel("Algun tipo de Lesion ");
		lblAlgunTipoDe.setForeground(new Color(88, 88, 250));
		lblAlgunTipoDe.setBounds(523, 85, 180, 14);
		panel_2.add(lblAlgunTipoDe);
		
		txtExplicaTuLesion = new JTextField();
		txtExplicaTuLesion.setText("Explica tu lesion");
		txtExplicaTuLesion.setBounds(561, 107, 86, 20);
		panel_2.add(txtExplicaTuLesion);
		txtExplicaTuLesion.setColumns(10);
		
		JRadioButton radioButton_1 = new JRadioButton("Dos Veces por semana  ");
		radioButton_1.setBackground(new Color(252, 252, 54));
		radioButton_1.setBounds(35, 247, 144, 23);
		panel_2.add(radioButton_1);
		
		JRadioButton radioButton_2 = new JRadioButton("Si todos los dias ");
		radioButton_2.setBackground(new Color(252, 252, 54));
		radioButton_2.setBounds(35, 299, 105, 23);
		panel_2.add(radioButton_2);
		
		JLabel lblDisponibilidadDeEjercicio = new JLabel("Disponibilidad de ejercicio semanal");
		lblDisponibilidadDeEjercicio.setForeground(new Color(88, 88, 250));
		lblDisponibilidadDeEjercicio.setBounds(40, 199, 180, 14);
		panel_2.add(lblDisponibilidadDeEjercicio);
		
		JRadioButton rdbtnTresVecesPor = new JRadioButton("Tres Veces por semana  ");
		rdbtnTresVecesPor.setBackground(new Color(252, 252, 54));
		rdbtnTresVecesPor.setBounds(35, 273, 144, 23);
		panel_2.add(rdbtnTresVecesPor);
		
		JRadioButton rdbtnUnaVecesPor = new JRadioButton("Una Vez por semana  ");
		rdbtnUnaVecesPor.setBackground(new Color(252, 252, 54));
		rdbtnUnaVecesPor.setBounds(35, 220, 144, 23);
		panel_2.add(rdbtnUnaVecesPor);
		
		JLabel lblPeso = new JLabel("Peso");
		lblPeso.setForeground(new Color(88, 88, 250));
		lblPeso.setBounds(259, 251, 37, 14);
		panel_2.add(lblPeso);
		
		txtIndicaTuPeso = new JTextField();
		txtIndicaTuPeso.setText("Indica tu peso");
		txtIndicaTuPeso.setColumns(10);
		txtIndicaTuPeso.setBounds(306, 248, 97, 20);
		panel_2.add(txtIndicaTuPeso);
		
		txtIndicaTuEdad = new JTextField();
		txtIndicaTuEdad.setText("Indica tu edad");
		txtIndicaTuEdad.setColumns(10);
		txtIndicaTuEdad.setBounds(306, 223, 97, 20);
		panel_2.add(txtIndicaTuEdad);
		
		JLabel lblEdad = new JLabel("Edad");
		lblEdad.setForeground(new Color(88, 88, 250));
		lblEdad.setBounds(259, 226, 37, 14);
		panel_2.add(lblEdad);
		
		txtIndicaTuAltura = new JTextField();
		txtIndicaTuAltura.setText("Indica tu altura");
		txtIndicaTuAltura.setColumns(10);
		txtIndicaTuAltura.setBounds(305, 276, 98, 20);
		panel_2.add(txtIndicaTuAltura);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setForeground(new Color(88, 88, 250));
		lblAltura.setBounds(258, 279, 37, 14);
		panel_2.add(lblAltura);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(88, 88, 250));
		panel.setBounds(0, 44, 653, 6);
		panel_2.add(panel);
		
		JLabel lblNewLabel_1 = new JLabel("Logo");
		lblNewLabel_1.setIcon(new ImageIcon(Formulario_inicial.class.getResource("/documentos/LOGOTIPO/favicon1.jpg")));
		lblNewLabel_1.setBounds(518, 232, 119, 90);
		panel_2.add(lblNewLabel_1);
		
		JButton btnEnviarDatos = new JButton("enviar datos");
		btnEnviarDatos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				PERFILCLIENTE lol=new PERFILCLIENTE();
				lol.setVisible(true);
				setVisible(false);
			}
		});
		btnEnviarDatos.setBounds(523, 195, 105, 23);
		panel_2.add(btnEnviarDatos);
		
	}
}
