package vistas;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import WIP_Gabriel.SQL;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



public class Rutinas extends JFrame {
	private JPanel contentPane;
	private JTextField txtDniDelCliente;
	private JTextField txtId;
	private JTextField txtNuevoValor;
	private JTextField txtAtributo;
	private JTextField txtEjercicio;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField txtDniEntrenador;
	private JTextField txtDniCliente;
	private JTextField txtDia;
	private JTable table;
	private String[][]clientes =new String[5][11];
	public Rutinas() {
		
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 800, 600);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			
			
			JPanel panel = new JPanel();
			panel.setBackground(new Color(88, 88, 250));
			panel.setBounds(0, 0, 140, 400);
			panel.setLayout(null);
			contentPane.add(panel);
			
			JLabel lblNewLabel = new JLabel("New label");
			lblNewLabel.setBounds(10, 11, 120, 130);
			panel.add(lblNewLabel);
			lblNewLabel.setBorder((Border) new LineBorder(new Color(0, 0, 0)));
			
			JButton btnMiPerfil = new JButton("Mi Perfil");
			btnMiPerfil.setBounds(25, 163, 89, 23);
			panel.add(btnMiPerfil);
			
			JButton btnClientes = new JButton("Clientes");
			btnClientes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					Clientes Clientes1=new Clientes();
					Clientes1.setVisible(true);
					setVisible(false);
					
				}
			});
			btnClientes.setBounds(10, 218, 120, 23);
			panel.add(btnClientes);
			
			
			
			
			JButton btnEntrenadores = new JButton("Entrenadores");
			
			
			btnEntrenadores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					Entrenador Entrenador1=new Entrenador();
					Entrenador1.setVisible(true);
					setVisible(false);
				}
			});
			btnEntrenadores.setBounds(10, 248, 120, 23);
			panel.add(btnEntrenadores);
			
			JButton btnEjercicios = new JButton("Ejercicios");
			btnEjercicios.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					Ejercicios Ejercicios1=new Ejercicios();
					Ejercicios1.setVisible(true);
				}
			});
			btnEjercicios.setBounds(10, 278, 120, 23);
			panel.add(btnEjercicios);
			
			JButton btnRutinas = new JButton("Rutinas");
			btnRutinas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					Rutinas Rutinas1=new Rutinas();
					Rutinas1.setVisible(true);
					setVisible(false);
				}
			});
			btnRutinas.setBounds(10, 307, 120, 23);
			panel.add(btnRutinas);
			
			final JPanel panel_2 = new JPanel();
			panel_2.setBackground(new Color(252, 252, 94));
			panel_2.setBounds(140, 71, 634, 490);
			contentPane.add(panel_2);
			panel_2.setLayout(null);
			
			txtDniDelCliente = new JTextField();
			txtDniDelCliente.setBounds(136, 11, 86, 20);
			txtDniDelCliente.setText("dni del cliente");
			panel_2.add(txtDniDelCliente);
			txtDniDelCliente.setColumns(10);
			
			JButton btnConsultarRutina = new JButton("Consultar Rutina");
			btnConsultarRutina.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					SQL conectar=new SQL();
					String a =null;
					try {
						conectar.SQLConnection("javaconnect", "root","");
						
						table = new JTable();
				 		table.setModel(new DefaultTableModel(
				 				new Object[][] {
				 					{clientes[0][0]="EJ1", clientes[0][1]="EJ2", clientes[0][2]="EJ3",clientes[0][3]="EJ4",clientes[0][4]="EJ5",clientes[0][5]="EJ6",clientes[0][6]="EJ7",clientes[0][7]="EJ8",clientes[0][8]="CLIENTE_ID",clientes[0][9]="ENTRENADOR",clientes[0][10]="DIAS"},
				 					{conectar.getidEJ1(txtDniDelCliente.getText()),conectar.getidEJ2(txtDniDelCliente.getText()),conectar.getidEJ3(txtDniDelCliente.getText()),conectar.getidEJ4(txtDniDelCliente.getText()),conectar.getidEJ5(txtDniDelCliente.getText()),conectar.getidEJ6(txtDniDelCliente.getText()),conectar.getidEJ7(txtDniDelCliente.getText()),conectar.getidEJ8(txtDniDelCliente.getText()),conectar.getidCLIENTE(txtDniDelCliente.getText()),conectar.getidENTRENADOR(txtDniDelCliente.getText()),conectar.getidDIAS(txtDniDelCliente.getText())}
				 				},
				 				new String[] {
				 					"EJ1", "EJ2", "EJ3", "EJ4", "EJ5", "EJ6", "EJ7", "EJ8","DNI CLIENTE","DNI ENTRENADOR","DIAS"
				 				}
				 			));
				 		table.setBounds(0, 40, 634, 34);
				 		panel_2.add(table);
						table.repaint();
						
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					
					
					
					conectar.closeConnection();
				}
			});
			btnConsultarRutina.setBounds(289, 10, 128, 23);
			panel_2.add(btnConsultarRutina);
			
			txtId = new JTextField();
			txtId.setBounds(10, 111, 86, 20);
			txtId.setText("id");
			panel_2.add(txtId);
			txtId.setColumns(10);
			
			txtNuevoValor = new JTextField();
			txtNuevoValor.setBounds(10, 142, 86, 20);
			txtNuevoValor.setText("Nuevo Valor");
			panel_2.add(txtNuevoValor);
			txtNuevoValor.setColumns(10);
			
			txtAtributo = new JTextField();
			txtAtributo.setBounds(10, 173, 86, 20);
			txtAtributo.setText("atributo");
			panel_2.add(txtAtributo);
			txtAtributo.setColumns(10);
			
			JButton btnEditar = new JButton("Editar");
			btnEditar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					
					SQL conectar=new SQL();
					try {
						conectar.SQLConnection("javaconnect", "root","");
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					conectar.edittuplaRUTINAS(txtId.getText(),txtAtributo.getText(), txtNuevoValor.getText());
					
					
					conectar.closeConnection();
				}
			});
			
			
			
			btnEditar.setBounds(133, 141, 89, 23);
			panel_2.add(btnEditar);
			
			txtEjercicio = new JTextField();
			txtEjercicio.setBounds(136, 210, 86, 20);
			txtEjercicio.setText("Ejercicio");
			panel_2.add(txtEjercicio);
			txtEjercicio.setColumns(10);
			
			textField_1 = new JTextField();
			textField_1.setText("Ejercicio");
			textField_1.setColumns(10);
			textField_1.setBounds(136, 281, 86, 20);
			panel_2.add(textField_1);
			
			textField_2 = new JTextField();
			textField_2.setText("Ejercicio");
			textField_2.setColumns(10);
			textField_2.setBounds(136, 317, 86, 20);
			panel_2.add(textField_2);
			
			textField_3 = new JTextField();
			textField_3.setText("Ejercicio");
			textField_3.setColumns(10);
			textField_3.setBounds(136, 348, 86, 20);
			panel_2.add(textField_3);
			
			textField_4 = new JTextField();
			textField_4.setText("Ejercicio");
			textField_4.setColumns(10);
			textField_4.setBounds(136, 241, 86, 20);
			panel_2.add(textField_4);
			
			textField_5 = new JTextField();
			textField_5.setText("Ejercicio");
			textField_5.setColumns(10);
			textField_5.setBounds(136, 379, 86, 20);
			panel_2.add(textField_5);
			
			textField_6 = new JTextField();
			textField_6.setText("Ejercicio");
			textField_6.setColumns(10);
			textField_6.setBounds(263, 210, 86, 20);
			panel_2.add(textField_6);
			
			textField_7 = new JTextField();
			textField_7.setText("Ejercicio");
			textField_7.setColumns(10);
			textField_7.setBounds(263, 241, 86, 20);
			panel_2.add(textField_7);
			
			txtDniEntrenador = new JTextField();
			txtDniEntrenador.setText("DNI Cliente");
			txtDniEntrenador.setBounds(263, 281, 86, 20);
			panel_2.add(txtDniEntrenador);
			txtDniEntrenador.setColumns(10);
			
			txtDniCliente = new JTextField();
			txtDniCliente.setBounds(263, 317, 86, 20);
			panel_2.add(txtDniCliente);
			txtDniCliente.setText("DNI Entrenador");
			txtDniCliente.setColumns(10);
			
			txtDia = new JTextField();
			txtDia.setText("Dia");
			txtDia.setBounds(263, 348, 86, 20);
			panel_2.add(txtDia);
			txtDia.setColumns(10);
			
			
			
			JButton btnAgregarNuevaRutina = new JButton("Agregar nueva rutina");
			btnAgregarNuevaRutina.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					SQL conectar=new SQL();
					try {
						conectar.SQLConnection("javaconnect", "root","");
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					conectar.insertDataRutinas2( txtEjercicio.getText(), textField_1.getText(), textField_2.getText(), textField_3.getText(), textField_4.getText(), textField_5.getText(), textField_6.getText(), textField_7.getText(), txtDniEntrenador.getText(), txtDniCliente.getText(),txtDia.getText());
					
					
					conectar.closeConnection();
					
					
				}
			});
					
					
			
			btnAgregarNuevaRutina.setBounds(410, 280, 158, 57);
			panel_2.add(btnAgregarNuevaRutina);
			

			JLabel label = new JLabel("Bienvenido a TRAINING RANK");
			
			label.setMaximumSize(new Dimension(200, 14));
			label.setForeground(new Color(88, 88, 250));
			label.setFont(new Font("Tahoma", Font.PLAIN, 18));
			label.setBounds(150, 17, 266, 22);
			
			contentPane.add(label);
	}

}