package vistas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import WIP_Gabriel.SQL;
import vistas.Menu;

public class LogIn extends JFrame {

	private JPanel contentPane;
	private JTextField txtWelcomeToTraining;
	public static JTextField txtUsuario;
	public static JTextField txtConstrasea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogIn frame = new LogIn();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LogIn() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		setTitle("Training Rank");
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("Button.disabledShadow"));
		panel.setBounds(55, 41, 497, 61);
		contentPane.add(panel);
		panel.setLayout(null);
		
		txtWelcomeToTraining = new JTextField();
		txtWelcomeToTraining.setBorder(null);
		txtWelcomeToTraining.setFont(new Font("Tahoma", Font.PLAIN, 29));
		txtWelcomeToTraining.setForeground(new Color(88, 88,225));
		txtWelcomeToTraining.setHorizontalAlignment(SwingConstants.CENTER);
		txtWelcomeToTraining.setBounds(10, 11, 477, 39);
		
		txtWelcomeToTraining.setText("WELCOME TO TRAINING RANK");
		
		panel.add(txtWelcomeToTraining);
		txtWelcomeToTraining.setColumns(10);
		
		panel = new JPanel();
		panel.setBackground(new Color(88, 88, 225));
		panel.setForeground(new Color(0, 0, 0));
		panel.setBounds(10, 148, 564, 10);
		contentPane.add(panel);
		
		
		JLabel lblSingIn = new JLabel("Sign In");
		lblSingIn.setHorizontalAlignment(SwingConstants.CENTER);
		lblSingIn.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblSingIn.setBounds(201, 113, 167, 45);
		
		contentPane.add(lblSingIn);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(252, 252, 54));
		panel_2.setBounds(10, 175, 564, 175);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		
		final JTextField txtUsuario = new JTextField();
		txtUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		txtUsuario.setText("User");
		txtUsuario.setBounds(208, 38, 158, 20);
		panel_2.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		final JPasswordField txtConstrasea = new JPasswordField();
		txtConstrasea.setHorizontalAlignment(SwingConstants.CENTER);
		txtConstrasea.setText("*********");
		txtConstrasea.setBounds(208, 80, 158, 20);
		panel_2.add(txtConstrasea);
		txtConstrasea.setColumns(10);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			String	valor="";
				
				
				   SQL conectar=new SQL();
					try {
						conectar.SQLConnection("javaconnect", "root","");
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					valor=conectar.LOGIN(txtUsuario.getText(),txtConstrasea.getText());
					
					switch (valor){
					case "entrenador":{
						Menu menu=new Menu();
						menu.setVisible(true);
						setVisible(false);
						break;

					}case "cliente": {
						Formulario_inicial menu=new Formulario_inicial();
						menu.setVisible(true);
						setVisible(false);
						break;
					}
					
					                    
					        
					            
					}System.out.println(valor);
						conectar.closeConnection();
			}
		});
		btnNewButton.setBounds(247, 111, 89, 23);
		panel_2.add(btnNewButton);
	}

}
