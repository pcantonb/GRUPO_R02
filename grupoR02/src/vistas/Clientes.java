package vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import WIP_Gabriel.SQL;

import javax.swing.SwingConstants;

public class Clientes extends JFrame {

	private JPanel contentPane;
	private JTextField txtDni;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtApellido_1;
	private JTextField txtDireccin;
	private JTextField txtTe;
	private JTextField txtCuentaCorriente;
	private JTextField txtSearch;
	private JTextField txtPassword;
	private JTextField txtRol;

	/**
	 * Launch the application.
	 */
	/**public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clientes frame = new Clientes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Clientes() {
		setBackground(UIManager.getColor("Button.background"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Training Rank");
		JPanel panel = new JPanel();
		panel.setBounds(10, 0, 140, 400);
		panel.setBackground(new Color(88, 88, 250));
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblImagen = new JLabel("IMAGEN");
		lblImagen.setForeground(UIManager.getColor("Button.focus"));
		lblImagen.setOpaque(true);
		lblImagen.setBackground(UIManager.getColor("Button.background"));
		lblImagen.setHorizontalAlignment(SwingConstants.CENTER);
		lblImagen.setBounds(10, 11, 120, 130);
		panel.add(lblImagen);
		lblImagen.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		JButton btnNewButton = new JButton("Mi Perfil");
		btnNewButton.setBackground(UIManager.getColor("Button.focus"));
		
		btnNewButton.setBounds(20, 152, 97, 23);
		panel.add(btnNewButton);
		
		JButton btnClientes = new JButton("Clientes");
		btnClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Clientes Clientes1=new Clientes();
				Clientes1.setVisible(true);

				
			}
		});
		btnClientes.setBounds(10, 218, 120, 23);
		panel.add(btnClientes);
		
		
		
		
		JButton btnEntrenadores = new JButton("Entrenadores");
		
		
		btnEntrenadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Entrenador Entrenador1=new Entrenador();
				Entrenador1.setVisible(true);
				setVisible(false);
			}
		});
		btnEntrenadores.setBounds(10, 248, 120, 23);
		panel.add(btnEntrenadores);
		
		JButton btnEjercicios = new JButton("Ejercicios");
		btnEjercicios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Ejercicios Ejercicios1=new Ejercicios();
				Ejercicios1.setVisible(true);
				setVisible(false);
			}
		});
		btnEjercicios.setBounds(10, 278, 120, 23);
		panel.add(btnEjercicios);
		
		JButton btnRutinas = new JButton("Rutinas");
		btnRutinas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Rutinas Rutinas1=new Rutinas();
				Rutinas1.setVisible(true);
				setVisible(false);
			}
		});
		btnRutinas.setBounds(10, 307, 120, 23);
		panel.add(btnRutinas);
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(136, 50, 638, 10);
		panel_1.setBackground(new Color(88, 88, 250));
		contentPane.add(panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(160, 71, 624, 490);
		panel_2.setBackground(new Color(252, 252, 54));
		contentPane.add(panel_2);
		
		JButton btnNewButton_5 = new JButton("Crear Usuario");
		btnNewButton_5.setBounds(456, 419, 116, 23);
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SQL conectar=new SQL();
				try {
					conectar.SQLConnection("javaconnect", "root","");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				conectar.insertDataPassClientes(txtDni.getText(), txtPassword.getText(), txtRol.getText());
				conectar.insertDataClientes(txtDni.getText(), txtNombre.getText(), txtApellido.getText(), txtApellido_1.getText(),txtDireccin.getText(),txtTe.getText(),txtCuentaCorriente.getText());
				
				conectar.closeConnection();
				
			}
		});
		panel_2.setLayout(null);
		panel_2.add(btnNewButton_5);
		
		txtDni = new JTextField();
		txtDni.setBounds(10, 45, 195, 20);
		txtDni.setText("DNI");
		panel_2.add(txtDni);
		txtDni.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(10, 86, 195, 20);
		txtNombre.setText("NOMBRE");
		txtNombre.setColumns(10);
		panel_2.add(txtNombre);
		
		txtApellido = new JTextField();
		txtApellido.setBounds(10, 127, 195, 20);
		txtApellido.setText("APELLIDO");
		txtApellido.setColumns(10);
		panel_2.add(txtApellido);
		
		txtApellido_1 = new JTextField();
		txtApellido_1.setBounds(10, 171, 195, 20);
		txtApellido_1.setText("2\u00BA APELLIDO");
		txtApellido_1.setColumns(10);
		panel_2.add(txtApellido_1);
		
		txtDireccin = new JTextField();
		txtDireccin.setBounds(10, 218, 195, 20);
		txtDireccin.setText("DIRECCI\u00D3N");
		txtDireccin.setColumns(10);
		panel_2.add(txtDireccin);
		
		txtTe = new JTextField();
		txtTe.setBounds(10, 261, 195, 20);
		txtTe.setText("TEL\u00C9FONO");
		txtTe.setColumns(10);
		panel_2.add(txtTe);
		
		txtCuentaCorriente = new JTextField();
		txtCuentaCorriente.setBounds(10, 306, 195, 20);
		txtCuentaCorriente.setText("Cuenta Corriente");
		txtCuentaCorriente.setColumns(10);
		panel_2.add(txtCuentaCorriente);
		
		txtPassword = new JTextField();
		txtPassword.setText("Password");
		txtPassword.setColumns(10);
		txtPassword.setBounds(10, 346, 195, 20);
		panel_2.add(txtPassword);
		
		txtRol = new JTextField();
		txtRol.setText("ROL");
		txtRol.setColumns(10);
		txtRol.setBounds(10, 380, 195, 20);
		panel_2.add(txtRol);
		
		JLabel lblNewLabel = new JLabel("Registro Cliente");
		lblNewLabel.setForeground(new Color(88, 88, 250));
		lblNewLabel.setBounds(162, 25, 140, 22);
		lblNewLabel.setMaximumSize(new Dimension(200, 14));
		lblNewLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); 
		contentPane.add(lblNewLabel);
		
		txtSearch = new JTextField();
		txtSearch.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		txtSearch.setText("Search...");
		txtSearch.setBounds(688, 11, 86, 20);
		contentPane.add(txtSearch);
		
		txtSearch.setColumns(10);
		
	}
}
