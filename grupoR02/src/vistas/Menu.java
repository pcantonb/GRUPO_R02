package vistas;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import WIP_Gabriel.SQL;

import javax.swing.JTable;

public class Menu extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private String[][]clientes =new String[2][7];
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(88, 88, 250));
		panel.setBounds(0, 0, 140, 400);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Imagen");
		lblNewLabel.setBounds(10, 11, 120, 130);
		panel.add(lblNewLabel);
		lblNewLabel.setBorder((Border) new LineBorder(new Color(0, 0, 0)));
		
		JButton btnMiPerfil = new JButton("Mi Perfil");
		btnMiPerfil.setBounds(25, 163, 89, 23);
		panel.add(btnMiPerfil);
		
		JButton btnClientes = new JButton("Clientes");
		btnClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Clientes Clientes1=new Clientes();
				Clientes1.setVisible(true);
				
			}
		});
		btnClientes.setBounds(10, 218, 120, 23);
		panel.add(btnClientes);
		
		JButton btnEntrenadores = new JButton("Entrenadores");
		btnEntrenadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Entrenador entrenador1=new Entrenador();
				entrenador1.setVisible(true);
				
			}
		});
		btnEntrenadores.setBounds(10, 248, 120, 23);
		panel.add(btnEntrenadores);
		
		JButton btnEjercicios = new JButton("Ejercicios");
		btnEjercicios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Ejercicios ejercicio1=new Ejercicios();
				ejercicio1.setVisible(true);
				
				
			}
		});
		btnEjercicios.setBounds(10, 278, 120, 23);
		panel.add(btnEjercicios);
		
		JButton btnRutinas = new JButton("Rutinas");
		btnRutinas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Rutinas rutina1=new Rutinas();
				rutina1.setVisible(true);
						
			}
		});
		btnRutinas.setBounds(10, 307, 120, 23);
		panel.add(btnRutinas);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(88, 88, 250));
		panel_1.setBounds(140, 50, 634, 10);
		contentPane.add(panel_1);
		
		final JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(252, 252, 94));
		panel_2.setBounds(140, 71, 634, 490);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(228, 352, 133, 127);
		label_1.setIcon(new ImageIcon(Menu.class.getResource("/documentos/LOGOTIPO/favicon1.jpg")));
		panel_2.add(label_1);
		
		JLabel lblBuscadorDeClientes = new JLabel("Buscador de Clientes");
		lblBuscadorDeClientes.setBounds(228, 21, 144, 22);
		lblBuscadorDeClientes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_2.add(lblBuscadorDeClientes);
		
		JLabel lblIntroduzcaElDni = new JLabel("Introduzca el DNI del Cliente:");
		lblIntroduzcaElDni.setBounds(53, 54, 204, 14);
		lblIntroduzcaElDni.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel_2.add(lblIntroduzcaElDni);
		
		final JTextField textField_1 = new JTextField();
		textField_1.setBounds(228, 54, 126, 20);
		panel_2.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL conectar=new SQL();
				//Menu m=new Menu();
				String a =null;
				try {
					conectar.SQLConnection("javaconnect", "root","");
					a=conectar.getID(textField_1.getText());
					table = new JTable();
			 		table.setModel(new DefaultTableModel(
			 				new Object[][] {
			 					{clientes[0][0]="DNI", clientes[0][1]="NOMBRE", clientes[0][2]="APELLIDO1",clientes[0][3]="APELLIDO2",clientes[0][4]="DIRECCION",clientes[0][5]="TELEFONO",clientes[0][6]="CUENTA_CORRIENTE"},
			 					{conectar.getID(textField_1.getText()), conectar.getnombre(textField_1.getText()),conectar.getidAPELLIDO1(textField_1.getText()),conectar.getidAPELLIDO2(textField_1.getText()),conectar.getidDIRECCION(textField_1.getText()),conectar.getidTELEFONO(textField_1.getText()),conectar.getidCUENTA_CORRIENTE(textField_1.getText())},
			 				},
			 				new String[] {
			 					"DNI", "Nombre", "Primer Apellido", "Segundo Apellido", "Direccion", "Telefono", "Cuenta corriente"
			 				}
			 			));
			 		table.setBounds(0, 171, 634, 34);
			 		panel_2.add(table);
					table.repaint();
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				
				
				conectar.closeConnection();
			}
		});
		btnBuscar.setBounds(364, 51, 89, 23);
		panel_2.add(btnBuscar);
		
		JLabel lblBuscadorDeEntrenadores = new JLabel("Buscador de Entrenador");
		lblBuscadorDeEntrenadores.setBounds(200, 102, 155, 22);
		lblBuscadorDeEntrenadores.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_2.add(lblBuscadorDeEntrenadores);
		
		JLabel lblIntroduzcaElDni_1 = new JLabel("Introduzca el DNI del Entrenador:");
		lblIntroduzcaElDni_1.setBounds(30, 139, 204, 14);
		lblIntroduzcaElDni_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel_2.add(lblIntroduzcaElDni_1);
		
		final JTextField textField_2 = new JTextField();
		textField_2.setBounds(228, 137, 126, 20);
		textField_2.setColumns(10);
		panel_2.add(textField_2);
		
		JButton button = new JButton("Buscar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SQL conectar=new SQL();
				//Menu m=new Menu();
				String a =null;
				try {
					conectar.SQLConnection("javaconnect", "root","");
					a=conectar.getIDE(textField_2.getText());
					table = new JTable();
			 		table.setModel(new DefaultTableModel(
			 				new Object[][] {
			 					{clientes[0][0]="DNI", clientes[0][1]="NOMBRE", clientes[0][2]="APELLIDO1",clientes[0][3]="APELLIDO2",clientes[0][4]="DIRECCION",clientes[0][5]="TELEFONO",clientes[0][6]="CUENTA CORRIENTE"},
			 					{conectar.getIDE(textField_2.getText()),conectar.getnombreE(textField_2.getText()),conectar.getidAPELLIDO1E(textField_2.getText()),conectar.getidAPELLIDO2E(textField_2.getText()),conectar.getidDIRECCIONE(textField_2.getText()),conectar.getidTELEFONOE(textField_2.getText()),conectar.getidCUENTA_CORRIENTEE(textField_2.getText())},
			 				},
			 				new String[] {
			 					"DNI", "Nombre", "Primer Apellido", "Segundo Apellido", "Direccion", "Telefono", "Cuenta corriente"
			 				}
			 			));
			 		table.setBounds(0, 171, 634, 34);
			 		panel_2.add(table);
					table.repaint();
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				
				
				conectar.closeConnection();
			}
		});
		
		button.setBounds(364, 136, 89, 23);
		panel_2.add(button);
		
		/////////////////////////////////////
		SQL db=new SQL();
		   try {
			db.SQLConnection("javaconnect", "root", " ");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		   String a=db.getID(textField_1.getText());
		   //ar[1]=db.getID("7787878");
		   //System.out.println(a);
		   //System.out.println(ar[1]);
		   //db.getVal("14124124");
		   //db.closeConnection();
		
		
		
		////////////////////////////////////
		//table = new JTable();
		//table.setModel(new DefaultTableModel(
		//	));
		//table.setBounds(0, 171, 634, 34);
		//panel_2.add(table);
		
		JTextField textField = new JTextField();
		textField.setText("Search...");
		textField.setColumns(10);
		textField.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		textField.setBounds(688, 19, 86, 20);
		contentPane.add(textField);
		
		JLabel label = new JLabel("Bienvenido a TRAINING RANK");
		label.setMaximumSize(new Dimension(200, 14));
		label.setForeground(new Color(88, 88, 250));
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label.setBounds(150, 17, 266, 22);
		contentPane.add(label);
		
	}
}