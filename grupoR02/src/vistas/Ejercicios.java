package vistas;

import java.awt.BorderLayout;
import WIP_Gabriel.SQL;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;


import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Ejercicios extends JFrame {

	private JPanel contentPane;
	private JTextField txtPuntos;
	private JTextField txtCambio;
	private JTextField txtNombreid;
	private JTextField txtDescripcion;
	private JTable table;
	private String[][]clientes =new String[5][8];
	private JTextField txtNombre_1;
	/**
	 * Launch the application.
	 */
	/**public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicios frame = new Ejercicios();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Ejercicios() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(88, 88, 250));
		panel.setBounds(0, 0, 140, 400);
		panel.setLayout(null);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(10, 11, 120, 130);
		panel.add(lblNewLabel);
		lblNewLabel.setBorder((Border) new LineBorder(new Color(0, 0, 0)));
		
		JButton btnMiPerfil = new JButton("Mi Perfil");
		btnMiPerfil.setBounds(25, 163, 89, 23);
		panel.add(btnMiPerfil);
		
		JButton btnClientes = new JButton("Clientes");
		btnClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Clientes Clientes1=new Clientes();
				Clientes1.setVisible(true);
				setVisible(false);
				
			}
		});
		btnClientes.setBounds(10, 218, 120, 23);
		panel.add(btnClientes);
		
		
		
		
		JButton btnEntrenadores = new JButton("Entrenadores");
		
		
		btnEntrenadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Entrenador Entrenador1=new Entrenador();
				Entrenador1.setVisible(true);
				setVisible(false);
			}
		});
		btnEntrenadores.setBounds(10, 248, 120, 23);
		panel.add(btnEntrenadores);
		
		JButton btnEjercicios = new JButton("Ejercicios");
		btnEjercicios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Ejercicios Ejercicios1=new Ejercicios();
				Ejercicios1.setVisible(true);
			}
		});
		btnEjercicios.setBounds(10, 278, 120, 23);
		panel.add(btnEjercicios);
		
		JButton btnRutinas = new JButton("Rutinas");
		btnRutinas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Rutinas Rutinas1=new Rutinas();
				Rutinas1.setVisible(true);
				setVisible(false);
			}
		});
		btnRutinas.setBounds(10, 307, 120, 23);
		panel.add(btnRutinas);
		
		final JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(252, 252, 94));
		panel_2.setBounds(140, 71, 634, 490);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JButton btnAgregarEjercicio = new JButton("Editar Tabla");
		
		
		
		final JTextField txtPeso = new JTextField();
		txtPeso.setText("Peso");
		txtPeso.setColumns(10);
		
		
		final JTextField txtPuntos=new JTextField();
		txtPuntos.setText("Puntos");
		txtPuntos.setColumns(10);
		
		
		
		final JTextField txtRepeticiones = new JTextField();
		txtRepeticiones.setText("Repeticiones");
		txtRepeticiones.setColumns(10);
		
		final JTextField txtTiempo = new JTextField();
		txtTiempo.setText("Tiempo");
		txtTiempo.setColumns(10);
		
		final JTextField txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setColumns(10);
		
		final JTextField txtDescripcin = new JTextField();
		txtDescripcin.setText("Descripci\u00F3n");
		txtDescripcin.setColumns(10);
		
		JButton btnAgregarNuevoEjercicio = new JButton("Agregar nuevo ejercicio");
	
		

		
		JLabel lblAgregarNuevoEjercicio = new JLabel("Agregar nuevo ejercicio");
		lblAgregarNuevoEjercicio.setForeground(new Color(88, 88, 250));
		lblAgregarNuevoEjercicio.setFont(new Font("Tahoma", Font.PLAIN, 24));
		
		txtCambio = new JTextField();
		txtCambio.setText("column name");
		txtCambio.setColumns(10);
		
		txtNombreid = new JTextField();
		txtNombreid.setText("nuevo valor");
		txtNombreid.setColumns(10);
		
		txtDescripcion = new JTextField();
		txtDescripcion.setText("campo");
		txtDescripcion.setColumns(10);
		
		JButton btnConsultarTabla = new JButton("Consultar Ejercicio");
		btnConsultarTabla.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SQL conectar=new SQL();
				String a =null;
				try {
					conectar.SQLConnection("javaconnect", "root","");
					
					table = new JTable();
			 		table.setModel(new DefaultTableModel(
			 				new Object[][] {
			 					{clientes[0][0]="ID", clientes[0][1]="TIEMPO", clientes[0][2]="NOMBRE",clientes[0][3]="PESO",clientes[0][4]="REPETICIONES",clientes[0][5]="DESCRIPCION",clientes[0][6]="PUNTOS"},
			 					{conectar.getidEJERCICIO(txtNombre_1.getText()),conectar.getTIEMPO(txtNombre_1.getText()),conectar.getNOMBRE(txtNombre_1.getText()),conectar.getPESO(txtNombre_1.getText()),conectar.getidREPETICIONES(txtNombre_1.getText()),conectar.getDESCRIPCION(txtNombre_1.getText()),conectar.getPUNTOS(txtNombre_1.getText())}
			 				},
			 				new String[] {
			 					"ID", "Tiempo", "Nombre", "Peso", "Repeticiones", "Puntos", "Descripcion"
			 				}
			 			));
			 		table.setBounds(0, 40, 634, 34);
			 		panel_2.add(table);
					table.repaint();
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				
				
				conectar.closeConnection();
				
			}
		});
		
		txtNombre_1 = new JTextField();
		txtNombre_1.setText("Nombre");
		txtNombre_1.setColumns(10);
		
		
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(50)
							.addComponent(txtNombre_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnConsultarTabla)
							.addGap(381))
						.addGroup(Alignment.LEADING, gl_panel_2.createSequentialGroup()
							.addGap(60)
							.addComponent(lblAgregarNuevoEjercicio))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(28)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_2.createSequentialGroup()
									.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel_2.createSequentialGroup()
											.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING, false)
												.addComponent(txtTiempo)
												.addComponent(txtPeso)
												.addComponent(txtRepeticiones))
											.addGap(73)
											.addComponent(btnAgregarNuevoEjercicio, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
										.addComponent(txtPuntos, GroupLayout.PREFERRED_SIZE, 322, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(Alignment.TRAILING, gl_panel_2.createSequentialGroup()
									.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
										.addComponent(txtDescripcin, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
										.addGroup(gl_panel_2.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(txtNombre, GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)))
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addComponent(txtNombreid, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtCambio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtDescripcion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnAgregarEjercicio))
							.addGap(100)))
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnConsultarTabla)
						.addComponent(txtNombre_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(134)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(18)
							.addComponent(lblAgregarNuevoEjercicio)
							.addGap(16)
							.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtDescripcin, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnAgregarEjercicio)))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtCambio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtNombreid, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtDescripcion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(txtPeso, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(20)
							.addComponent(txtRepeticiones, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(btnAgregarNuevoEjercicio, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(txtTiempo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(txtPuntos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
			panel_2.setLayout(gl_panel_2);
			
			JLabel label = new JLabel("Bienvenido a TRAINING RANK");
			label.setMaximumSize(new Dimension(200, 14));
			label.setForeground(new Color(88, 88, 250));
			label.setFont(new Font("Tahoma", Font.PLAIN, 18));
			label.setBounds(150, 17, 266, 22);
			
			contentPane.add(label);
			
			JTextField textField = new JTextField();
			textField.setText("Search...");
			textField.setColumns(10);
			textField.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			textField.setBounds(688, 19, 86, 20);
			contentPane.add(textField);
			
			btnAgregarNuevoEjercicio.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					SQL conectar=new SQL();
					try {
						conectar.SQLConnection("javaconnect", "root","");
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					conectar.insertDataEjercicios(txtNombre.getText(),txtDescripcin.getText(),txtPeso.getText(), txtRepeticiones.getText(),txtTiempo.getName(), txtPuntos.getText());
					
					conectar.closeConnection();
					
				}
			});
	
			
			
			
			btnAgregarEjercicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SQL conectar=new SQL();
				try {
					conectar.SQLConnection("javaconnect", "root","");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				conectar.edittupla(txtCambio.getText(),txtDescripcion.getText(),txtNombreid.getText());
				
				
				conectar.closeConnection();
			}
		});
		}
	}

